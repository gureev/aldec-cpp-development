#ifndef __CONTAINERS_FIXED_ARRAY_HPP__
#define __CONTAINERS_FIXED_ARRAY_HPP__

//------------------------------------------------------------------------------

namespace Containers {

//------------------------------------------------------------------------------

#define GET_SIZE( _A ) ( sizeof( _A ) / sizeof( _A[ 0 ] ) )

template< typename _T, std::size_t _N >
constexpr std::size_t getSize( _T( & )[ _N ] )
{
	return _N;
}

template< typename _T, std::size_t _N >
constexpr std::size_t getSize( _T( * )[ _N ] )
{
	return _N;
}

//------------------------------------------------------------------------------

} // namespace Containers

  //------------------------------------------------------------------------------

#endif // __CONTAINERS_FIXED_ARRAY_HPP__
