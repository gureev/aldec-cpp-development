#include "ph/ph.hpp"

#include "containers/dynamic_array.hpp"
#include "containers/fixed_array.hpp"
#include "containers/proxy_array.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

namespace Containers::Test {

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_DynamicArray_Empty )
{
	DynamicArray< int > ar( 10 );

	ASSERT( ar.getSize() == 0 );
	ASSERT( ar.getCapacity() == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_DynamicArray_Single )
{
	DynamicArray< int > ar( 10 );
	ar.push_back( 5 );

	ASSERT( ar.getSize() == 1 );
	ASSERT( ar.getCapacity() == 10 );
	ASSERT( ar[ 0 ] == 5 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_DynamicArray_Several )
{
	DynamicArray< int > ar( 10 );
	ar.push_back( 5 );
	ar.push_back( 7 );

	ASSERT( ar.getSize() == 2 );
	ASSERT( ar.getCapacity() == 10 );
	ASSERT( ar[ 0 ] == 5 );
	ASSERT( ar[ 1 ] == 7 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_DynamicArray_Read )
{
	DynamicArray< int > ar( 10 );
	ar.push_back( 5 );
	ar.push_back( 7 );

	int v1 = ar[ 0 ];
	int v2 = ar[ 1 ];

	ASSERT( ar.getSize() == 2 );
	ASSERT( ar.getCapacity() == 10 );
	ASSERT( v1 == 5 );
	ASSERT( v2 == 7 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_DynamicArray_Write )
{
	DynamicArray< int > ar( 10 );
	ar.push_back( 0 );
	ar.push_back( 0 );
	ar[ 0 ] = 5;
	ar[ 1 ] = 7;

	ASSERT( ar.getSize() == 2 );
	ASSERT( ar.getCapacity() == 10 );
	ASSERT( ar[ 0 ] == 5 );
	ASSERT( ar[ 1 ] == 7 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_DynamicArray_SeveralWrites )
{
	DynamicArray< int > ar( 10 );
	ar.push_back( 0 );
	ar.push_back( 0 );
	ar[ 0 ] = 5;
	ar[ 1 ] = ar[ 0 ] = 10;

	ASSERT( ar.getSize() == 2 );
	ASSERT( ar.getCapacity() == 10 );
	ASSERT( ar[ 0 ] == 10 );
	ASSERT( ar[ 1 ] == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_DynamicArray_EmptyPopBack )
{
	DynamicArray< int > ar( 10 );

	ASSERT_THROWS( ar.pop_back(), DynamicArray< int >::ArrayUnderflow );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_DynamicArray_PushPop )
{
	DynamicArray< int > ar( 10 );
	ar.push_back( 5 );
	ar.pop_back();

	ASSERT( ar.getSize() == 0 );
	ASSERT( ar.getCapacity() == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_DynamicArray_PushPopSeveralTimes )
{
	DynamicArray< int > ar( 10 );
	ar.push_back( 5 );
	ar.push_back( 7 );
	ar.pop_back();
	ar.pop_back();

	ASSERT( ar.getSize() == 0 );
	ASSERT( ar.getCapacity() == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_DynamicArray_LessPopThanPush )
{
	DynamicArray< int > ar( 10 );
	ar.push_back( 9 );
	ar.push_back( 11 );
	ar.pop_back();

	ASSERT( ar.getSize() == 1 );
	ASSERT( ar.getCapacity() == 10 );
	ASSERT( ar[ 0 ] == 9 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_DynamicArray_CustomClass )
{
	struct A
	{
		A()
		{
			std::cout << "A::A" << std::endl;
		}
		A( const A & )
		{
			std::cout << "A::A<copy>" << std::endl;
		}
		A( A && )
		{
			std::cout << "A::A<move>" << std::endl;
		}
		~A()
		{
			std::cout << "A::~A" << std::endl;
		}
		A & operator = ( const A & )
		{
			std::cout << "A:: = <copy>" << std::endl;
			return *this;
		}
		A & operator = ( A && )
		{
			std::cout << "A:: = <move>" << std::endl;
			return *this;
		}
		int m_v{ 0 };
	};

	std::cout << std::endl;

	DynamicArray< A > ar( 10 );
	ar.push_back( A() );

	std::cout << std::endl;

	ar.emplace_back();

	std::cout << std::endl;

	ASSERT( ar.getSize() == 2 );
	ASSERT( ar.getCapacity() == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( Containers_FixedArray_Size )
{
	const int a[] = { 1, 2, 3, 4, 5 };
	const std::size_t aSize1 = sizeof( a ) / sizeof( a[ 0 ] );
	const std::size_t aSize21 = GET_SIZE( a );
	const std::size_t aSize22 = GET_SIZE( &a );
	const std::size_t aSize31 = getSize( a );
	const std::size_t aSize32 = getSize( &a );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_ProxyArray_Empty )
{
	ProxyArray< int > ar( 10 );

	ASSERT( ar.getSize() == 0 );
	ASSERT( ar.getCapacity() == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_ProxyArray_Single )
{
	ProxyArray< int > ar( 10 );
	ar.push_back( 5 );

	ASSERT( ar.getSize() == 1 );
	ASSERT( ar.getCapacity() == 10 );
	ASSERT( ar[ 0 ] == 5 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_ProxyArray_Several )
{
	ProxyArray< int > ar( 10 );
	ar.push_back( 5 );
	ar.push_back( 7 );

	ASSERT( ar.getSize() == 2 );
	ASSERT( ar.getCapacity() == 10 );
	ASSERT( ar[ 0 ] == 5 );
	ASSERT( ar[ 1 ] == 7 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_ProxyArray_Read )
{
	ProxyArray< int > ar( 10 );
	ar.push_back( 5 );
	ar.push_back( 7 );

	int v1 = ar[ 0 ];
	int v2 = ar[ 1 ];

	ASSERT( ar.getSize() == 2 );
	ASSERT( ar.getCapacity() == 10 );
	ASSERT( v1 == 5 );
	ASSERT( v2 == 7 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_ProxyArray_Write )
{
	ProxyArray< int > ar( 10 );
	ar.push_back( 0 );
	ar.push_back( 0 );
	ar[ 0 ] = 5;
	ar[ 1 ] = 7;

	ASSERT( ar.getSize() == 2 );
	ASSERT( ar.getCapacity() == 10 );
	ASSERT( ar[ 0 ] == 5 );
	ASSERT( ar[ 1 ] == 7 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_ProxyArray_SeveralWrites )
{
	ProxyArray< int > ar( 10 );
	ar.push_back( 0 );
	ar.push_back( 0 );
	ar[ 0 ] = 5;
	ar[ 1 ] = ar[ 0 ] = 10;

	ASSERT( ar.getSize() == 2 );
	ASSERT( ar.getCapacity() == 10 );
	ASSERT( ar[ 0 ] == 10 );
	ASSERT( ar[ 1 ] == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_ProxyArray_EmptyPopBack )
{
	ProxyArray< int > ar( 10 );

	ASSERT_THROWS( ar.pop_back(), ProxyArray< int >::ArrayUnderflow );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_ProxyArray_PushPop )
{
	ProxyArray< int > ar( 10 );
	ar.push_back( 5 );
	ar.pop_back();

	ASSERT( ar.getSize() == 0 );
	ASSERT( ar.getCapacity() == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_ProxyArray_PushPopSeveralTimes )
{
	ProxyArray< int > ar( 10 );
	ar.push_back( 5 );
	ar.push_back( 7 );
	ar.pop_back();
	ar.pop_back();

	ASSERT( ar.getSize() == 0 );
	ASSERT( ar.getCapacity() == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_ProxyArray_LessPopThanPush )
{
	ProxyArray< int > ar( 10 );
	ar.push_back( 9 );
	ar.push_back( 11 );
	ar.pop_back();

	ASSERT( ar.getSize() == 1 );
	ASSERT( ar.getCapacity() == 10 );
	ASSERT( ar[ 0 ] == 9 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_ProxyArray_CustomClass )
{
	struct A
	{
		A()
		{
			std::cout << "A::A" << std::endl;
		}
		A( const A & )
		{
			std::cout << "A::A<copy>" << std::endl;
		}
		A( A && )
		{
			std::cout << "A::A<move>" << std::endl;
		}
		~A()
		{
			std::cout << "A::~A" << std::endl;
		}
		A & operator = ( const A & )
		{
			std::cout << "A:: = <copy>" << std::endl;
			return *this;
		}
		A & operator = ( A && )
		{
			std::cout << "A:: = <move>" << std::endl;
			return *this;
		}
		int m_v{ 0 };
	};

	std::cout << std::endl;

	ProxyArray< A > ar( 10 );
	ar.push_back( A() );

	std::cout << std::endl;

	ar.emplace_back();

	std::cout << std::endl;

	ASSERT( ar.getSize() == 2 );
	ASSERT( ar.getCapacity() == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_ProxyVector )
{
	std::vector< bool > v;
	v.emplace_back( true );

	std::vector< bool >::reference r = v.back();
	r.flip();

	ASSERT( v.front() == false );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ContainersTest_TransparentComparator )
{
	std::map< std::string, int > m1;
	m1[ "hello" ] = 10;
	ASSERT( m1.count( "hello" ) ); // std::string is implicitly built
	// count( const std::string & )

	std::map< std::string, int, std::less<> > m2;
	m2[ "hello" ] = 10;
	ASSERT( m2.count( "hello" ) ); // std::string is not built
	// count( const char (&)[ 6 ] )

	// arithmetic operations, comparisons, logical operations, bitwise operations
}

//------------------------------------------------------------------------------

namespace ContainersTest_EmplaceBackReturnsValue
{
	struct X
	{
		void action()
		{
		}
		void wait()
		{
		}
	};
	std::unordered_map< std::string, std::unique_ptr< X > > ready14;
	std::vector< std::unique_ptr< X > > wait14;

	auto f14( const std::string & _string, std::unique_ptr< X > _x )
	{
		auto it = ready14.find( _string );
		if ( it == ready14.end() )
		{
			auto result = ready14.emplace( _string, std::move( _x ) );
			result.first->second->action();
			return 1;
		}
		else
		{
			wait14.push_back( std::move( _x ) );
			wait14.back()->wait();
			return 2;
		}
	}
}

DECLARE_TEST_CASE( ContainersTest_EmplaceBackReturnsValue_CPP14 )
{
	std::string key = "hello";

	auto r1 = f14( key, std::make_unique< ContainersTest_EmplaceBackReturnsValue::X >() );
	ASSERT( r1 == 1 );

	auto r2 = f14( key, std::make_unique< ContainersTest_EmplaceBackReturnsValue::X >() );
	ASSERT( r2 == 2 );
}

//------------------------------------------------------------------------------

namespace ContainersTest_EmplaceBackReturnsValue
{
	std::unordered_map< std::string, std::unique_ptr< X > > ready17;
	std::vector< std::unique_ptr< X > > wait17;

	auto f17( const std::string & _string, std::unique_ptr< X > _x )
	{
		if (
			auto [ pos, inserted ] = ready17.try_emplace( _string, std::move( _x ) );
			inserted
		)
		{
			pos->second->action();
			return 1;
		}
		else
		{
			wait17.emplace_back( std::move( _x ) )->wait();
			return 2;
		}
	}
}

DECLARE_TEST_CASE( ContainersTest_EmplaceBackReturnsValue_CPP17 )
{
	std::string key = "hello";

	auto r1 = f17( key, std::make_unique< ContainersTest_EmplaceBackReturnsValue::X >() );
	ASSERT( r1 == 1 );

	auto r2 = f17( key, std::make_unique< ContainersTest_EmplaceBackReturnsValue::X >() );
	ASSERT( r2 == 2 );
}

//------------------------------------------------------------------------------

} // namespace Containers::Test
