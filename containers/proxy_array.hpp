#ifndef __CONTAINERS_PROXY_ARRAY_HPP__
#define __CONTAINERS_PROXY_ARRAY_HPP__

//------------------------------------------------------------------------------

#include "meta/is_class.hpp"
#include "meta/policy/class_destructor.hpp"
#include "meta/policy/default_destructor.hpp"

//------------------------------------------------------------------------------

namespace Containers {

//------------------------------------------------------------------------------

template< typename T >
class ProxyArray
{

//------------------------------------------------------------------------------

public:

//------------------------------------------------------------------------------

	class ProxyElement
	{
		T * m_element;

	public:

		ProxyElement() = delete;

		ProxyElement( T & _element )
		{
			m_element = & _element;
		}

		ProxyElement( const T & _element ) = delete;

		ProxyElement( const ProxyElement & _other )
		{
			( * m_element ) = * _other.m_element;
		}
		ProxyElement & operator = ( const ProxyElement & _other )
		{
			( * m_element ) = * _other.m_element;
			return * this;
		}
		ProxyElement & operator = ( const T & _element )
		{
			( * m_element ) = _element;
			return * this;
		}

		operator T () const noexcept
		{
			return * m_element;
		}
	};

//------------------------------------------------------------------------------

	class ArrayOverflow {};
	class ArrayUnderflow {};

//------------------------------------------------------------------------------

	explicit ProxyArray( int _capacity )
		:	m_size( 0 )
		,	m_capacity( _capacity )
	{
		m_data = operator new ( sizeof( T ) * m_capacity );
	}

	~ProxyArray()
	{
		while ( getSize() )
			pop_back();

		operator delete ( m_data );
	}

//------------------------------------------------------------------------------

	int getSize() const
	{
		return m_size;
	}

	int getCapacity() const
	{
		return m_capacity;
	}

//------------------------------------------------------------------------------

	ProxyElement operator [] ( int _index )
	{
		assert( _index >= 0 );
		assert( _index < m_size );
		return ProxyElement{ storage()[ _index ] };
	}

//------------------------------------------------------------------------------

	const ProxyElement operator [] ( int _index ) const
	{
		assert( _index >= 0 );
		assert( _index < m_size );
		return ProxyElement{ storage()[ _index ] };
	}

//------------------------------------------------------------------------------

	void push_back( const T & _v )
	{
		emplace_back( _v );
	}

	template< typename ... Args >
	void emplace_back( Args && ... _args )
	{
		if ( m_size == m_capacity )
			throw ArrayOverflow();

		new ( storage() + m_size ) T( std::forward< Args >( _args )... );
		++m_size;
	}

//------------------------------------------------------------------------------

	void pop_back()
	{
		if ( !m_size )
			throw ArrayUnderflow();

		using Destructor = std::conditional_t<
				Meta::IsClass< T >::value
			,	Meta::Policy::ClassDestructor< T >
			,	Meta::Policy::DefaultDestructor< T >
		>;
		Destructor::Destruct( storage() + m_size );
		--m_size;
	}

//------------------------------------------------------------------------------

private:

//------------------------------------------------------------------------------

	T * storage()
	{
		return static_cast< T * >( m_data );
	}

//------------------------------------------------------------------------------

private:

//------------------------------------------------------------------------------

	void * m_data;
	int m_size;
	int m_capacity;

//------------------------------------------------------------------------------

};

//------------------------------------------------------------------------------

} // namespace Containers

//------------------------------------------------------------------------------

#endif // __CONTAINERS_PROXY_ARRAY_HPP__
