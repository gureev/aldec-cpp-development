#ifndef __ALLOCATORS_MEMORY_MANAGER_HPP__
#define __ALLOCATORS_MEMORY_MANAGER_HPP__

//------------------------------------------------------------------------------

namespace Allocators {

//------------------------------------------------------------------------------

class MemoryManager
{

//------------------------------------------------------------------------------

public:

//------------------------------------------------------------------------------

	void * allocate( std::size_t /*_size*/ )
	{
		std::cout << "MemoryManager::allocate" << std::endl;
		return nullptr;
	}

	void deallocate( void * /*_data*/ )
	{
		std::cout << "MemoryManager::deallocate" << std::endl;
	}

//------------------------------------------------------------------------------

};

//------------------------------------------------------------------------------

} // namespace Allocators

//------------------------------------------------------------------------------

void * operator new ( std::size_t _size, Allocators::MemoryManager & _manager )
{
	return _manager.allocate( _size );
}
void operator delete ( void * _data, Allocators::MemoryManager & _manager )
{
	_manager.deallocate( _data );
}

//------------------------------------------------------------------------------

#endif // __ALLOCATORS_MEMORY_MANAGER_HPP__
