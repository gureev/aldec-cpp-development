#ifndef __ALLOCATORS_CUSTOM_ALLOCATOR_HPP__
#define __ALLOCATORS_CUSTOM_ALLOCATOR_HPP__

//------------------------------------------------------------------------------

namespace Allocators {

//------------------------------------------------------------------------------

template< typename T >
struct CustomAllocator
{

//------------------------------------------------------------------------------

	using value_type = T;
	using pointer = value_type * ;

//------------------------------------------------------------------------------

	CustomAllocator() = default;

	CustomAllocator( const CustomAllocator & ) = default;

	template< typename U >
	CustomAllocator( const U & )
	{
	}

//------------------------------------------------------------------------------

	pointer allocate( std::size_t _size )
	{
		std::cout << "CA::allocate" << std::endl;
		return static_cast< pointer >( ::operator new( _size * sizeof( T ) ) );
	}

//------------------------------------------------------------------------------

	void deallocate( pointer _p, std::size_t /*_size*/ )
	{
		std::cout << "CA::deallocate" << std::endl;
		::operator delete( _p );
	}

//------------------------------------------------------------------------------

};

//------------------------------------------------------------------------------

} // namespace Allocators

//------------------------------------------------------------------------------

#endif // __ALLOCATORS_CUSTOM_ALLOCATOR_HPP__
