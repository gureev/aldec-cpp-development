#ifndef __ALLOCATORS_CUSTOM_DELETER_HPP__
#define __ALLOCATORS_CUSTOM_DELETER_HPP__

//------------------------------------------------------------------------------

namespace Allocators {

//------------------------------------------------------------------------------

template< typename T >
struct CustomDeleter
{

//------------------------------------------------------------------------------

	using value_type = T;
	using pointer = value_type * ;

//------------------------------------------------------------------------------

	CustomDeleter() = default;

	CustomDeleter( const CustomDeleter & ) = default;

	template< typename U >
	CustomDeleter( const U & )
	{
	}

//------------------------------------------------------------------------------

	void operator () ( pointer _p )
	{
		std::cout << "CD::delete" << std::endl;
		::operator delete( _p );
	}

//------------------------------------------------------------------------------

};

//------------------------------------------------------------------------------

} // namespace Allocators

  //------------------------------------------------------------------------------

#endif // __ALLOCATORS_CUSTOM_DELETER_HPP__
