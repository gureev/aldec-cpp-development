#include "ph/ph.hpp"

#include "allocators/global_new.hpp"
#include "allocators/memory_manager.hpp"
#include "allocators/custom_allocator.hpp"
#include "allocators/custom_deleter.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

namespace Allocators::Test {

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_ScalarNew )
{
	int * pInt = new int;
	delete pInt;
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_VectorNew )
{
	int * pIntA = new int[ 10 ];
	delete [] pIntA;
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_ScalarPlacementNew )
{
	int buf;
	void * pBuf = new ( &buf ) int();
	operator delete ( pBuf, &buf );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_CustomScalarPlacementNew )
{
	int buf;
	void * pBufCustom = new ( &buf, 25 ) int();
	operator delete ( pBufCustom, &buf, 25 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_VectorPlacementNew )
{
	int buf[3];
	void * pBuf = new ( buf ) int[ 3 ];
	operator delete [] ( pBuf, buf );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_CustomVectorPlacementNew )
{
	int buf[ 3 ];
	void * pBuf = new ( buf, 25 ) int[ 3 ];
	operator delete [] ( pBuf, buf, 25 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_CustomScalarNew )
{
	int * pInt = new ( 1, 2, 3 ) int;
	delete pInt;
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_ClassSpecificScalarNew )
{
	struct MyClass
	{
		void * operator new ( std::size_t _size )
		{
			std::cout << "MyClass::operator new" << std::endl;
			return ::operator new( _size );
		}
		void operator delete ( void * _ptr )
		{
			std::cout << "MyClass::operator delete" << std::endl;
			return ::operator delete( _ptr );
		}
	};
	struct MyAnotherClass
	{
	};

	MyClass * p1 = new MyClass;
	delete p1;

	MyAnotherClass * p2 = new MyAnotherClass;
	delete p2;

	MyClass * p3 = new MyClass[ 2 ];
	delete[] p3;

	MyAnotherClass * p4 = new MyAnotherClass[ 2 ];
	delete[] p4;
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_ClassSpecificVectorNew )
{
	struct MyClass
	{
		void * operator new [] ( std::size_t _size )
		{
			std::cout << "MyClass::operator new []" << std::endl;
			return ::operator new( _size );
		}
		void operator delete [] ( void * _ptr )
		{
			std::cout << "MyClass::operator delete []" << std::endl;
			return ::operator delete( _ptr );
		}
	};
	struct MyAnotherClass
	{
	};

	MyClass * p1 = new MyClass;
	delete p1;

	MyAnotherClass * p2 = new MyAnotherClass;
	delete p2;

	MyClass * p3 = new MyClass[ 2 ];
	delete[] p3;

	MyAnotherClass * p4 = new MyAnotherClass[ 2 ];
	delete[] p4;
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_ClassSpecificPolymorphicCall_NonVirtualDestructor )
{
	struct MyClass
	{
	};
	struct MyAnotherClass : public MyClass
	{
		void * operator new ( std::size_t _size )
		{
			std::cout << "MyAnotherClass::operator new" << std::endl;
			return ::operator new( _size );
		}
		void operator delete ( void * _ptr )
		{
			std::cout << "MyAnotherClass::operator delete" << std::endl;
			return ::operator delete( _ptr );
		}
	};

	MyClass * p1 = new MyAnotherClass;
	delete p1; // Global operator delete // behavior is undefined
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_ClassSpecificPolymorphicCall_OnlyDerived )
{
	struct MyClass
	{
		virtual ~MyClass() = default;
	};
	struct MyAnotherClass : public MyClass
	{
		void * operator new ( std::size_t _size )
		{
			std::cout << "MyAnotherClass::operator new" << std::endl;
			return ::operator new( _size );
		}
		void operator delete ( void * _ptr )
		{
			std::cout << "MyAnotherClass::operator delete" << std::endl;
			return ::operator delete( _ptr );
		}
	};

	MyClass * p1 = new MyAnotherClass;
	delete p1;
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_ClassSpecificPolymorphicCall_SameVersions )
{
	struct MyClass
	{
		virtual ~MyClass() = default;

		void * operator new ( std::size_t _size )
		{
			std::cout << "MyClass::operator new" << std::endl;
			return ::operator new( _size );
		}
			void operator delete ( void * _ptr )
		{
			std::cout << "MyClass::operator delete" << std::endl;
			return ::operator delete( _ptr );
		}
	};
	struct MyAnotherClass : public MyClass
	{
		void * operator new ( std::size_t _size )
		{
			std::cout << "MyAnotherClass::operator new" << std::endl;
			return ::operator new( _size );
		}
		void operator delete ( void * _ptr )
		{
			std::cout << "MyAnotherClass::operator delete" << std::endl;
			return ::operator delete( _ptr );
		}
	};

	MyClass * p1 = new MyAnotherClass;
	delete p1;
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_ClassSpecificPolymorphicCall_OnlyBase )
{
	struct MyClass
	{
		virtual ~MyClass() = default;

		void * operator new ( std::size_t _size )
		{
			std::cout << "MyClass::operator new" << std::endl;
			return ::operator new( _size );
		}
		void operator delete ( void * _ptr )
		{
			std::cout << "MyClass::operator delete" << std::endl;
			return ::operator delete( _ptr );
		}
	};
	struct MyAnotherClass : public MyClass
	{
	};

	MyClass * p1 = new MyAnotherClass;
	delete p1;
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_ClassSpecificPolymorphicCall_DifferentVersions )
{
	struct MyClass
	{
		virtual ~MyClass() = default;

		void * operator new ( std::size_t _size )
		{
			std::cout << "MyClass::operator new" << std::endl;
			return ::operator new( _size );
		}
		void operator delete ( void * _ptr, std::size_t _size )
		{
			std::cout << "MyClass::operator delete" << std::endl;
			return ::operator delete( _ptr, _size );
		}
	};
	struct MyAnotherClass : public MyClass
	{
		void * operator new ( std::size_t _size )
		{
			std::cout << "MyAnotherClass::operator new" << std::endl;
			return ::operator new( _size );
		}
			void operator delete ( void * _ptr )
		{
			std::cout << "MyAnotherClass::operator delete" << std::endl;
			return ::operator delete( _ptr );
		}
	};

	MyClass * p1 = new MyAnotherClass;
	delete p1;
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_LargeAlignment )
{
	struct alignas( __STDCPP_DEFAULT_NEW_ALIGNMENT__ ) SmallStruct
	{
		void * operator new ( std::size_t _size )
		{
			std::cout << "SmallStruct::operator new: " << _size << std::endl;
			return nullptr;
		}
		void * operator new ( std::size_t _size, std::align_val_t _align )
		{
			std::cout << "SmallStruct::operator new (aligned): " << _size << ' ' << static_cast< std::size_t >( _align ) << std::endl;
			return nullptr;
		}
	};
	struct alignas( __STDCPP_DEFAULT_NEW_ALIGNMENT__ * 2 ) LargeStruct
	{
		void * operator new ( std::size_t _size )
		{
			std::cout << "SmallStruct::operator new: " << _size << std::endl;
			return nullptr;
		}
		void * operator new ( std::size_t _size, std::align_val_t _align )
		{
			std::cout << "SmallStruct::operator new (aligned): " << _size << ' ' << static_cast< std::size_t >( _align ) << std::endl;
			return nullptr;
		}
	};
	struct alignas( __STDCPP_DEFAULT_NEW_ALIGNMENT__ * 2 ) LargeNonAlignStruct
	{
		void * operator new ( std::size_t _size )
		{
			std::cout << "SmallStruct::operator new: " << _size << std::endl;
			return nullptr;
		}
	};

	new SmallStruct; // new ( size )
	new LargeStruct; // new ( size, align )
	new LargeNonAlignStruct; // new ( size )
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_MemoryManager )
{
	MemoryManager mm;
	
	int * pInt1 = new int( 10 );
	delete pInt1;

	int * pInt2 = new ( mm ) int( 15 ); // placement new
	//pInt2->~int() // before placement delete
	operator delete( pInt2, mm );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_UniquePtr_MakeVsNew )
{
	struct X
	{
		void * operator new ( std::size_t _size )
		{
			std::cout << "X::new" << std::endl;
			return ::operator new( _size );
		}
		void operator delete ( void * _data )
		{
			std::cout << "X::delete" << std::endl;
			return ::operator delete( _data );
		}
	};

	{
		std::cout << "T1" << std::endl;
		std::unique_ptr< X > x1( new X{} );
	}
	{
		std::cout << "T2" << std::endl;
		std::make_unique< X >();
	}
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_SharedPtr_MakeVsNew )
{
	struct X
	{
		void * operator new ( std::size_t _size )
		{
			std::cout << "X::new" << std::endl;
			return ::operator new( _size );
		}
		void operator delete ( void * _data )
		{
			std::cout << "X::delete" << std::endl;
			return ::operator delete( _data );
		}
	};

	{
		std::cout << "T1" << std::endl;
		std::shared_ptr< X > x1( new X{} );
	}
	{
		std::cout << "T2" << std::endl;
		std::make_shared< X >(); // nothing is printed
	}
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_VectorCustomAllocator )
{
	std::vector< int, CustomAllocator< int > > v;
	v.emplace_back( 10 );
	v.push_back( 12 );

	ASSERT( v.size() == 2 );
	ASSERT( v.front() == 10 );
	ASSERT( v.back() == 12 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_SharedPtr_CustomAllocator )
{
	struct X
	{
	};

	{
		std::shared_ptr< X > x1{ new X{}, CustomDeleter< X >{} };
	}
	{
		std::shared_ptr< X > x2{ new X{}, CustomDeleter< X >{}, CustomAllocator< X >{} };
	}
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( AllocatorsTest_UniquePtr_CustomAllocator )
{
	struct X
	{
	};

	// NOTE: No version with allocator. No internal data to manage
	{
		std::unique_ptr< X, CustomDeleter< X > > x1{ new X{}, CustomDeleter< X >{} };
	}
}

//------------------------------------------------------------------------------

} // namespace Allocators::Test
