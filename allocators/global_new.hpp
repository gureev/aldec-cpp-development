#ifndef __ALLOCATORS_GLOBAL_NEW_HPP__
#define __ALLOCATORS_GLOBAL_NEW_HPP__

//------------------------------------------------------------------------------

//#define ENABLE_DUMP

//------------------------------------------------------------------------------

#ifdef ENABLE_DUMP
#define DUMP( _TEXT ) std::cout << _TEXT << std::endl;
#else
#define DUMP( _TEXT )
#endif

//------------------------------------------------------------------------------

void * operator new ( std::size_t _size )
{
	DUMP( "operator new" );
	return std::malloc( _size );
}

void * operator new ( std::size_t _size, int, int, int )
{
	DUMP( "operator new custom" );
	return std::malloc( _size );
}

void operator delete ( void * _ptr )
{
	DUMP( "operator delete" );
	std::free( _ptr );
}

// Sized has higher priority than non-sized
void operator delete ( void * _ptr, std::size_t /*_size*/ )
{
	DUMP( "operator delete (sized)" );
	std::free( _ptr );
}

//------------------------------------------------------------------------------

void * operator new [] ( std::size_t _size )
{
	DUMP( "operator new []" );
	return std::malloc( _size );
}

void operator delete [] ( void * _ptr )
{
	DUMP( "operator delete []" );
	std::free( _ptr );
}

// Sized has higher priority than non-sized
void operator delete [] ( void * _ptr, std::size_t /*_size*/ )
{
	DUMP( "operator delete [] (sized)" );
	std::free( _ptr );
}

//------------------------------------------------------------------------------

// Global DEFAULT placement new CANNOT be overloaded
void * operator new ( std::size_t _size, void * _ptr ) noexcept;
void * operator new [] ( std::size_t _size, void * _ptr ) noexcept;
void operator delete ( void * _ptr, void * _place ) noexcept;
void operator delete [] ( void * _ptr, void * _place ) noexcept;

//------------------------------------------------------------------------------

// Global CUSTOM placement new CAN be overloaded

void * operator new ( std::size_t /*_size*/, void *, int )
{
	DUMP( "operator new (placement) custom" );
	return nullptr;
}

void operator delete ( void * /*_ptr*/, void *, int )
{
	DUMP( "operator delete (placement) custom" );
}

//------------------------------------------------------------------------------

void * operator new [] ( std::size_t /*_size*/, void *, int )
{
	DUMP( "operator new [] (placement) custom" );
	return nullptr;
}

void operator delete [] ( void * /*_ptr*/, void *, int )
{
	DUMP( "operator delete [] (placement) custom" );
}

//------------------------------------------------------------------------------

#undef DUMP
#undef ENABLE_DUMP

//------------------------------------------------------------------------------

#endif // __ALLOCATORS_GLOBAL_NEW_HPP__
