#ifndef __DYNAMIC_LIBRARY_FUNCTIONS_HPP__
#define __DYNAMIC_LIBRARY_FUNCTIONS_HPP__

//------------------------------------------------------------------------------

#include "library/dynamic_library_export.hpp"

//------------------------------------------------------------------------------

extern "C" {

//------------------------------------------------------------------------------

int dynamic_library_without_export( int _value );

//------------------------------------------------------------------------------

DYNAMIC_LIBRARY_EXPORT
int dynamic_library_with_export( int _value );

//------------------------------------------------------------------------------

} // extern "C"

//------------------------------------------------------------------------------

#endif // __DYNAMIC_LIBRARY_FUNCTIONS_HPP__
