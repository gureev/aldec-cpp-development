#include "ph/ph.hpp"

#include "library/dynamic_library_class.hpp"

//------------------------------------------------------------------------------

DynamicLibraryClass::DynamicLibraryClass() = default;

//------------------------------------------------------------------------------

DynamicLibraryClass::~DynamicLibraryClass() = default;

//------------------------------------------------------------------------------

int
DynamicLibraryClass::getValue() const
{
	return 15;
}

//------------------------------------------------------------------------------

DynamicLibraryClass * createDynamicLibraryClass()
{
	return new DynamicLibraryClass;
}

//------------------------------------------------------------------------------
