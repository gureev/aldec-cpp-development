#include "ph/ph.hpp"

#include "library/static_library_functions.hpp"
#include "library/static_library_class.hpp"

#include "library/dynamic_library_functions.hpp"
#include "library/dynamic_library_class.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

namespace Library::Test {

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( LibraryTest_Static_UseExportFunction )
{
	int v = static_library_function( 10 );
	ASSERT( v == 40 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( LibraryTest_Static_UseExportClass )
{
	StaticLibraryClass c;
	int v = c.getValue();
	ASSERT( v == 5 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( LibraryTest_Dynamic1_UseExportFunction )
{
	//int v = dynamic_library_without_export( 10 );
	int v = dynamic_library_with_export( 10 );
	ASSERT( v == 80 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( LibraryTest_Dynamic1_UseExportClass )
{
	DynamicLibraryClass c;
	int v = c.getValue();
	ASSERT( v == 15 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( LibraryTest_Dynamic2_UseExportFunction )
{
	HINSTANCE libraryHandler = LoadLibrary( L"dynamic_lib_2.dll" );
	ASSERT( libraryHandler );

	using FunPtr = int ( * )( int );
	FunPtr f1 = reinterpret_cast< FunPtr >( GetProcAddress( libraryHandler, "dynamic_library_without_export" ) );
	FunPtr f2 = reinterpret_cast< FunPtr >( GetProcAddress( libraryHandler, "dynamic_library_with_export" ) );

	ASSERT( !f1 );
	ASSERT( f2 );

	int v = f2( 1 );
	ASSERT( v == 8 );

	FreeLibrary( libraryHandler );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( LibraryTest_Dynamic2_UseExportClass )
{
	HINSTANCE libraryHandler = LoadLibrary( L"dynamic_lib_2.dll" );
	ASSERT( libraryHandler );

	/*
		There is no simple portable way to do this without function-helper
	*/

	using FunPtr = DynamicLibraryClass * ( * )();
	FunPtr f = reinterpret_cast< FunPtr >( GetProcAddress( libraryHandler, "createDynamicLibraryClass" ) );

	ASSERT( f );

	std::unique_ptr< DynamicLibraryClass > v( f() );
	ASSERT( v->getValue() == 15 );

	FreeLibrary( libraryHandler );
}

//------------------------------------------------------------------------------

} // namespace Library::Test
