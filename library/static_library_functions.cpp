#include "ph/ph.hpp"

#include "library/static_library_functions.hpp"

//------------------------------------------------------------------------------

int static_library_function( int _value )
{
	return _value << 2;
}

//------------------------------------------------------------------------------
