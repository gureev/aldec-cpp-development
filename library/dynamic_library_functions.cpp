#include "ph/ph.hpp"

#include "library/dynamic_library_functions.hpp"

//------------------------------------------------------------------------------

int dynamic_library_without_export( int _value )
{
	return _value << 1;
}

//------------------------------------------------------------------------------

DYNAMIC_LIBRARY_EXPORT
int dynamic_library_with_export( int _value )
{
	return _value << 3;
}

//------------------------------------------------------------------------------
