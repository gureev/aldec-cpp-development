#ifndef __DYNAMIC_LIBRARY_EXPORT_HPP__
#define __DYNAMIC_LIBRARY_EXPORT_HPP__

//------------------------------------------------------------------------------

/*
	Windows:	__declspec( dllexport )
				__declspec( dllimport )

	CYGWIN:		__attribute__ ((dllexport))
				__attribute__ ((dllimport))

	Exported:	__attribute__ ((visibility ("default")))
	Local:		__attribute__ ((visibility ("hidden")))
*/

#ifdef DYNAMIC_LIBRARY_PROJECT
	#define DYNAMIC_LIBRARY_EXPORT __declspec( dllexport )
#else
	#define DYNAMIC_LIBRARY_EXPORT __declspec( dllimport )
#endif

//------------------------------------------------------------------------------

#endif // __DYNAMIC_LIBRARY_EXPORT_HPP__
