#include "ph/ph.hpp"

#include "library/static_library_class.hpp"

//------------------------------------------------------------------------------

StaticLibraryClass::StaticLibraryClass() = default;

//------------------------------------------------------------------------------

StaticLibraryClass::~StaticLibraryClass() = default;

//------------------------------------------------------------------------------

int
StaticLibraryClass::getValue() const
{
	return 5;
}

//------------------------------------------------------------------------------
