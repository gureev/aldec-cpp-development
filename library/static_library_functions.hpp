#ifndef __STATIC_LIBRARY_FUNCTIONS_HPP__
#define __STATIC_LIBRARY_FUNCTIONS_HPP__

//------------------------------------------------------------------------------

int static_library_function( int _value );

//------------------------------------------------------------------------------

#endif // __STATIC_LIBRARY_FUNCTIONS_HPP__
