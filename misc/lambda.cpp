#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( Lambda_Mutable )
{
	auto x = [ z{ 0 } ]() mutable noexcept
	{
		return ++z;
	};
	ASSERT( x() == 1 );
	ASSERT( x() == 2 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( Lambda_PerfectForwarding )
{
	auto f = []( int )
	{
	};
	auto x = [ f ]( auto && _v ) noexcept
	{
		f( std::forward< decltype( _v ) >( _v ) );
	};
	x( 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( Lambda_PerfectForwarding_Variadic )
{
	auto f = []( int )
	{
	};
	auto x = [ f ]( auto && ... _v ) noexcept
	{
		f( std::forward< decltype( _v ) >( _v )... );
	};
	x( 10 );
}

//------------------------------------------------------------------------------
