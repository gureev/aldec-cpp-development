#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------


DECLARE_TEST_CASE( PhoenixSingleton_Idea )
{
	struct X
	{
		int x;
	};

	X x{ 10 };

	const X & rX{ x };
	assert( rX.x == 10 );

	x.~X(); // Phoenix Death
	new ( & x ) X{ 5 }; // Phoenix Rebirth

	assert( rX.x == 5 );
}

//------------------------------------------------------------------------------
