#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( PointerArithmetic )
{
	using T = int;

	T p[] = { 1, 2, 3, 4, 5, 6 };
	int N = 4;

	int r1 = p[ N ];
	int r2 = N[ p ];
	int r3 = *( p + N ); // *( p + N * sizeof( T ) )

	ASSERT( r1 == 5 );
	ASSERT( r2 == 5 );
	ASSERT( r3 == 5 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( UnionExample )
{
	struct short2
	{
		short m_1;
		short m_2;
	};
	union U
	{
		short2 m_short;
		int m_int;
	};
	static_assert( sizeof( U ) == 4 );

	U m_u;
	m_u.m_int = 20;
	ASSERT( m_u.m_int == 20 );

	m_u.m_short = { 10, 15 };
	ASSERT( m_u.m_int != 20 );

	m_u.m_short = { 0, 20 };
	ASSERT( m_u.m_int != 20 );

	m_u.m_short = { 20, 0 };
	ASSERT( m_u.m_int == 20 );
}

//------------------------------------------------------------------------------

constexpr auto dim1 = 10;
constexpr auto dim2 = 3;
constexpr auto dim3 = 17;
constexpr auto dim4 = 23;

int get( int * pData, int i, int j, int k, int z )
{
	return *( pData + i * dim2*dim3*dim4 + j * dim3*dim4 + k * dim4 + z );
}

DECLARE_TEST_CASE( ArrayIndex )
{
	int zero{ 10 };

	int a[ dim1 ][ dim2 ][ dim3 ][ dim4 ];
	int * pA = reinterpret_cast< int * >( a );

	for ( auto & a1 : a )
		for ( auto & a2 : a1 )
			for ( auto & a3 : a2 )
				for ( auto & v : a3 )
					v = zero++;

	for ( int i{ 0 }; i < dim1; ++i )
		for ( int j{ 0 }; j < dim2; ++j )
			for ( int k{ 0 }; k < dim3; ++k )
				for ( int z{ 0 }; z < dim4; ++z )
				{
					int v1 = a[ i ][ j ][ k ][ z ];
					int v2 = get( pA, i, j, k, z );
					assert( v1 == v2 );
				}
}

//------------------------------------------------------------------------------
