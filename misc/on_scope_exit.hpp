#ifndef __MISC_ON_SCOPE_EXIT_HPP__
#define __MISC_ON_SCOPE_EXIT_HPP__

//------------------------------------------------------------------------------

namespace Meta {

//------------------------------------------------------------------------------

template< typename T >
class OnScopeExit
{
	T m_action;

public:

	explicit OnScopeExit( const T & _action )
		:	m_action{ _action }
	{
	}

	~OnScopeExit()
	{
		m_action();
	}
};

#define CONCAT_IMPL( A, B ) A ## B
#define CONCAT( A, B ) CONCAT_IMPL( A, B )

//------------------------------------------------------------------------------

#define USE_UNIQUE_PTR

//------------------------------------------------------------------------------

#ifdef USE_UNIQUE_PTR

#define ON_SCOPE_EXIT( _ACTION )                                                   \
auto CONCAT( onScopeExitAction, __LINE__ ) = [action = _ACTION]( void * _data )    \
{                                                                                  \
   assert( reinterpret_cast< void * >( 0xDEAD ) == _data );                        \
   action();                                                                       \
};                                                                                 \
std::unique_ptr< void, decltype( CONCAT( onScopeExitAction, __LINE__ ) ) >         \
   CONCAT( onScopeExit, __LINE__ )(                                                \
          reinterpret_cast< void * >( 0xDEAD )                                     \
      ,   CONCAT( onScopeExitAction, __LINE__ )                                    \
   );

#else

#define ON_SCOPE_EXIT( _ACTION )                                              \
auto CONCAT( onScopeExitAction, __LINE__ ) = _ACTION;                         \
OnScopeExit< decltype( CONCAT( onScopeExitAction, __LINE__ ) ) >              \
   CONCAT( onScopeExit, __LINE__ )( CONCAT( onScopeExitAction, __LINE__ ) );

#endif

//------------------------------------------------------------------------------
	
} // namespace Meta

//------------------------------------------------------------------------------

#endif // __MISC_ON_SCOPE_EXIT_HPP__
