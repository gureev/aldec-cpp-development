#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

template< typename T >
class BittyPointer
{
	using Pointer = T * ;
	using Bitset = std::ptrdiff_t;

	static constexpr auto ms_lowBound{ 0 };
	static constexpr auto ms_highBound{ alignof(T)-1 };

	Pointer m_ptr{};

private:

	bool canStoreState( int _state ) const noexcept
	{
		return	static_cast< unsigned int >( _state ) >= ms_lowBound
			&&	static_cast< unsigned int >( _state ) <= ms_highBound;
	}

	void pack( Pointer _ptr, int _state ) noexcept
	{
		assert( canStoreState( _state ) );

		auto bitset{ reinterpret_cast< Bitset >( _ptr ) };
		bitset |= _state;
		m_ptr = reinterpret_cast< Pointer >( bitset );
	}

	using Tuple = std::tuple< Pointer, int >;
	Tuple unpack() const noexcept
	{
		auto bitset{ reinterpret_cast< Bitset >( m_ptr ) };

		auto state{ static_cast< int >( bitset & ms_highBound ) };
		auto ptr{ reinterpret_cast< Pointer >( bitset & ~ms_highBound ) };

		return { ptr, state };
	}

public:

	void set( Pointer _ptr, int _state ) noexcept
	{
		pack( _ptr, _state );
	}

	Pointer get() const noexcept
	{
		return std::get< 0 >( unpack() );
	}

	int state() const noexcept
	{
		return std::get< 1 >( unpack() );
	}

	int stateLimit() const noexcept
	{
		return ms_highBound;
	}
};

//------------------------------------------------------------------------------


DECLARE_TEST_CASE( BittyPointer_Test )
{
	struct X
	{
		int x;
	};

	std::srand( static_cast< unsigned int >( ::time( nullptr ) ) );

	static_assert( sizeof( BittyPointer< int > ) == sizeof( int * ) );
	static_assert( alignof( BittyPointer< int > ) == alignof( int * ) );

	using TestType = X;
	constexpr auto limit{ alignof( TestType ) };

	for ( int i{ 0 }; i < limit; ++i )
	{
		int v{ std::rand() };
		std::unique_ptr< TestType > pInt{ new TestType{ v } };

		BittyPointer< TestType > p;
		p.set( pInt.get(), i );

		assert( p.get() == pInt.get() );
		assert( p.state() == i );
		assert( p.stateLimit() == limit - 1 );
	}
}

//------------------------------------------------------------------------------
