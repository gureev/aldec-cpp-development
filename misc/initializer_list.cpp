#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( InitializerList_Constructor )
{
	struct X
	{
		int m_v{};

		X()
		{
			m_v = 1;
		}
		X( int, int )
		{
			m_v = 2;
		}
		X( std::string, int )
		{
			m_v = 3;
		}
		X( std::initializer_list< double >, bool )
		{
			m_v = 4;
		}
		X( std::initializer_list< double > )
		{
			m_v = 5;
		}
	};

	X x1{};
	X x2{ 5, 10 };
	X x3( 5, 10 );
	X x4{ "h", 15 }; // compiler may not enforce left-to-right evaluation order in braced initializer list
	X x5{ { 2.3, 2.4 }, true };
	X x6{{}};
	ASSERT( x1.m_v == 1 );
	ASSERT( x2.m_v == 5 );
	ASSERT( x3.m_v == 2 );
	ASSERT( x4.m_v == 3 );
	ASSERT( x5.m_v == 4 );
	ASSERT( x6.m_v == 5 );
}

//------------------------------------------------------------------------------
