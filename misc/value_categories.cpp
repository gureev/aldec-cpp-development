#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ValueCategories_lvalue )
{
	// expression whose evaluation determines the identity of an object whose resources CANNOT be reused

	int a{};
	std::cin;

	int b[ 5 ] = {};
	b[ 0 ];

	int c{};
	c = a;

	++c;

	// lvalue is no longer 'left side assignment expression'
	// const lvalue cannot appear at left side
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ValueCategories_prvalue )
{
	// prvalue expression whose evaluation initializes an object
	// or computes the value of the operand of an operator, as specified by the context in which it appears

	int a{};
	a + 5; // result is prvalue without object

	int c{};
	c++; // post-increment

	a < c;

	struct X
	{
	};
	X{} = X{}; // rvalue appears at left side
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ValueCategories_xvalue )
{
	// expression whose evaluation determines the identity of an object whose resources CAN be reused

	int a{};
	std::move( a );
	static_cast< int && >( a );

	int b[ 5 ] = {};
	b[ 0 ]; // only in case of 'b' is rvalue
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ValueCategories_PrvalueInCpp17 )
{
	struct X
	{
		X() = default;

		X( const X & ) = default;
		X & operator = ( const X & ) = default;

		//X( X && ) = delete;
		//X & operator = ( X && ) = delete;
	};

	auto x1 = X();
	auto x2 = [](){ return X{}; }();
	// C++ 03: X() is rvalue. Copy constructor is called (if no optimization is performed)
	// C++ 11: X() is prvalue. Move constructor is called. The move constructor must be valid, even if you don�t need
	// C++ 17: X() is prvalue. No move constructor is called, but 'x' is directly initialized as if 'x' was X()
}

//------------------------------------------------------------------------------

int dummy()
{
	return {};
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ValueCategories_Decltype )
{
	ASSERT( std::is_same< decltype( 5 ),		int >::value );
	ASSERT( std::is_same< decltype( ( 5 ) ),	int >::value );

	int x{};
	ASSERT( std::is_same< decltype( x ),		int >::value );
	ASSERT( std::is_same< decltype( ( x ) ),	int & >::value );

	int && xm = std::move( x );
	ASSERT( std::is_same< decltype( xm ),		int && >::value );
	ASSERT( std::is_same< decltype( ( xm ) ),	int & >::value );

	ASSERT( std::is_same< decltype( std::move( x ) ),		int && >::value );
	ASSERT( std::is_same< decltype( ( std::move( x ) ) ),	int && >::value );

	ASSERT( std::is_same< decltype( dummy() ),		int >::value );
	ASSERT( std::is_same< decltype( ( dummy() ) ),	int >::value );
}

//------------------------------------------------------------------------------
