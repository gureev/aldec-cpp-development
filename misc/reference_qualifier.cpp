#include "ph/ph.hpp"

#include "meta/is_same.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ReferenceQualifier_PrValue )
{
	struct X
	{
		int doAction() &
		{
			return 1;
		}
		int doAction() &&
		{
			return 2;
		}
	};

	auto build = []()
	{
		return X{};
	};

	X x;
	ASSERT( x.doAction() == 1 );
	ASSERT( X{}.doAction() == 2 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ReferenceQualifier_MoveFromClass )
{
	// NOTE: Use example with temporary materialization and move-semantic
	// NOTE: This example shows how to encapsulate and re-use same flow

	struct Y
	{
		Y()
		{
			std::cout << "Y::Y" << std::endl;
		}
		Y( const Y & )
		{
			std::cout << "Y::Y<copy>" << std::endl;
		}
		Y( Y && )
		{
			std::cout << "Y::Y<move>" << std::endl;
		}
	};

	struct X
	{
		using Data = std::vector< Y >;
		Data m_values;

		X()
		{
			m_values.emplace_back();
		}

		Data & data() &
		{
			return m_values;
		}

		Data && data() &&
		{
			return std::move( m_values );
		}
	};

	auto make = []()
	{
		return X{};
	};

	std::cout << '1' << std::endl;

	X x;
	auto r1 = x.data(); // copy
	ASSERT( Meta::IsSame< decltype( r1 ), X::Data >::value );

	std::cout << '2' << std::endl;

	auto r2 = make().data(); // move
	ASSERT( Meta::IsSame< decltype( r2 ), X::Data >::value );
}

//------------------------------------------------------------------------------
