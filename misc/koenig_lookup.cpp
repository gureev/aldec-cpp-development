#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

namespace A
{
	struct X
	{
	};
}

namespace C
{
	struct Y
	{
	};
}

namespace A
{
	void f( X, C::Y )
	{
	}
}

namespace C
{
	void f( A::X, Y )
	{
	}
}

//------------------------------------------------------------------------------

namespace B
{
	void f( A::X, C::Y )
	{
	}
	void q()
	{
		[[ maybe_unused ]] A::X x;
		[[ maybe_unused ]] C::Y y;
		//f( x, y ); // ambiguous call
	}
}

//------------------------------------------------------------------------------

struct D
{
	void f( A::X, C::Y ) // more powerful while lookup
	{
	}
	void q()
	{
		[[ maybe_unused ]] A::X x;
		[[ maybe_unused ]] C::Y y;
		f( x, y ); // D::f
	}
};

//------------------------------------------------------------------------------

namespace A1
{
	void f( int )
	{
	}
	void f( double )
	{
	}

	template< typename T >
	void g( T _t )
	{
		f( _t );
	}
}
namespace A2
{
	struct X
	{
	};
	void f( X )
	{
	}
}
namespace A2
{
	DECLARE_TEST_CASE( KoenigLookup_MultiResolution )
	{
		X x;
		A1::g( x ); // A2::f
		
		int i{};
		A1::g( i ); // A1::f

		double d{};
		A1::g( d ); // A1::f
	}
}

//------------------------------------------------------------------------------

namespace BaseClassNamespace
{
	struct Base
	{
	};
}
namespace DerivedClassNamespace
{
	struct Derived
		:	public BaseClassNamespace::Base
	{
	};
}
namespace BaseClassNamespace
{
	int funForDerived( const DerivedClassNamespace::Derived & )
	{
		return 5;
	}
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( KoenigLookup_FunctionFromNamespaceWithBaseClass )
{
	DerivedClassNamespace::Derived d;
	ASSERT( funForDerived( d ) == 5 );
}

//------------------------------------------------------------------------------

namespace ParameterClassNamespace
{
	struct Parameter
	{
	};
}
namespace BaseClassNamespace
{
	struct ParamHandler
	{
		void fun1( ParameterClassNamespace::Parameter )
		{
		}
		ParameterClassNamespace::Parameter fun2()
		{
			return {};
		}
	};
}
namespace ParameterClassNamespace
{
	void callMeNow1( void ( BaseClassNamespace::ParamHandler::* )( Parameter ) )
	{
	}
	void callMeNow2( Parameter( BaseClassNamespace::ParamHandler::* )() )
	{
	}
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( KoenigLookup_FunctionFromNamespaceWithParameter )
{
	auto ptr1 = & BaseClassNamespace::ParamHandler::fun1;
	auto ptr2 = & BaseClassNamespace::ParamHandler::fun2;
	callMeNow1( ptr1 );
	callMeNow2( ptr2 );
}

//------------------------------------------------------------------------------

namespace TemplateResolutionNamespace
{
	struct TRN
	{
	};

	void test1( TRN & )
	{
	}

	template< typename T >
	void test2( TRN & )
	{
	}
}

DECLARE_TEST_CASE( KoenigLookup_TemplateFunctionResolutionFailure )
{
	TemplateResolutionNamespace::TRN trn;
	test1( trn );
	//test2< int >( trn );
	/*
		+--> To apply ADL and find tes2 function, 'trn' should be treated as function argument.
		|	 To ensure 'trn' is function argument, '< int >' should be template argument.
		|	 To ensure '< int >' is template argument, 'test2' should be template function.
		|	 To ensure 'test2' is function, it should be found by applying ADL   >--+
		|																			|
		+---------------------------------------------------------------------------+
	*/
}

//------------------------------------------------------------------------------
