#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ConstCastError )
{
	struct A
	{
		void f( const int & _ref )
		{
			const_cast< int & >( _ref ) = 5;
		}
	};

	const int a = 10;
	ASSERT( a == 10 );
	A().f( a );

	ASSERT( a == 10 );
	//ASSERT( a == 5 );
}

//------------------------------------------------------------------------------
