#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

namespace Some
{
	namespace More
	{
		struct Y
		{
			int m_v{ 'y' };
		};
	}

	struct X
	{
		int m_v{ 'x' };
	};

	int value{ 17 };

	char f()
	{
		return 'f';
	}
}

DECLARE_TEST_CASE( Using )
{
	{
		using namespace Some::More;
		Y y;
		ASSERT( y.m_v == 'y' );
	}
	{
		using Some::X;
		X x;
		ASSERT( x.m_v == 'x' );
	}
	{
		using Some::value;
		ASSERT( value == 17 );
		value = 18;
		ASSERT( value == 18 );
	}
	{
		using Some::f;
		char r = f();
		ASSERT( r == 'f' );
	}
}

//------------------------------------------------------------------------------

template< typename T >
struct BXT
{
	using Type = T;

	template< typename U >
	struct Magic;

	void f1( Type )
	{
	}
	void f2( Magic< T > )
	{
	}
};
template< typename T >
struct DXT
	:	public BXT< T >
{
	using typename BXT< T >::Type;
	//using typename BXT< T >::template Magic; // Restricted by standard

	template< typename U >
	using Magic = typename BXT< T >::template Magic< U >;

	void f1( Type )
	{
	}
	void f2( Magic< T > _t )
	{
		_t.m_x = 10;
	}
};
template< typename T >
template< typename U >
struct BXT< T >::Magic
{
	int m_x{};
};

DECLARE_TEST_CASE( UsingTemplate )
{
	DXT< int > dxt;
	dxt.f1( 10 );
	dxt.f2( BXT< int >::Magic< int >{} );
}

//------------------------------------------------------------------------------
