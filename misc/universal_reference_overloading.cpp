#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

struct SomeClass
{
	template< typename T >
	explicit SomeClass( T && )
	{
		std::cout << "SomeClass: &&" << std::endl;
	}

	explicit SomeClass( int )
	{
		std::cout << "SomeClass: int" << std::endl;
	}

	SomeClass( const SomeClass & )
	{
		std::cout << "SomeClass: copy" << std::endl;
	}

	SomeClass( SomeClass && )
	{
		std::cout << "SomeClass: move" << std::endl;
	}
};

struct SomeSomeClass
	:	public SomeClass
{
	explicit SomeSomeClass( int _v )
		:	SomeClass( _v )
	{
	}

	SomeSomeClass( const SomeSomeClass & _other )
		:	SomeClass( _other )
	{
		std::cout << "SomeSomeClass: copy" << std::endl;
	}

	SomeSomeClass( SomeSomeClass && _other )
		:	SomeClass( std::move( _other ) )
	{
		std::cout << "SomeSomeClass: move" << std::endl;
	}
};

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( UniversalReferenceOverloading_Problem )
{
	std::string m1{};
	int m2{};
	short m3{};

	SomeClass s1{ m1 };
	SomeClass s2{ std::string( "some" ) };
	SomeClass s3{ "some" };
	SomeClass s4{ m2 };
	SomeClass s5{ 10 };
	SomeClass s6{ m3 }; // actually '&&', but logically expected 'int'
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( UniversalReferenceOverloading_Copy )
{
	SomeClass s1{ 10 };
	const SomeClass s2{ 10 };

	SomeClass s3{ s1 }; // actually '&&', but logically expected 'copy'
	SomeClass s4{ s2 };
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( UniversalReferenceOverloading_Inheritance )
{
	SomeSomeClass s1{ 10 };
	const SomeSomeClass s2{ 10 };

	SomeSomeClass s3{ s1 }; // actually '&&', but logically expected 'copy'
	SomeSomeClass s4{ s2 }; // actually '&&', but logically expected 'copy'
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( UniversalReferenceOverloading_Solution_PassConstReference )
{
	// NOTE: C++03 style

	struct SomeClassSolution
	{
		// NOTE: Should be used for each possible type
		SomeClassSolution( const std::string & )
		{
		}
		SomeClassSolution( const char * )
		{
		}
	};
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( UniversalReferenceOverloading_Solution_PassValue )
{
	struct SomeClassSolution
	{
		// NOTE: Should be used for each possible type
		SomeClassSolution( std::string )
			//	:	m_value( std::move() )
		{
		}
		SomeClassSolution( const char * )
		{
		}
	};
}

//------------------------------------------------------------------------------

struct SomeClassWithTag
{
	template< typename T >
	explicit SomeClassWithTag( T && _t )
		:	SomeClassWithTag(
					std::forward< T >( _t )
				,	std::is_integral< std::decay_t< T > >()
			)
	{
	}

	explicit SomeClassWithTag( int, std::true_type )
	{
		std::cout << "SomeClassWithTag: int (true_type)" << std::endl;
	}

	template< typename T >
	explicit SomeClassWithTag( T &&, std::false_type )
	{
		std::cout << "SomeClassWithTag: && (false_type)" << std::endl;
	}

	SomeClassWithTag( const SomeClassWithTag & )
	{
		std::cout << "SomeClassWithTag: copy" << std::endl;
	}

	SomeClassWithTag( SomeClassWithTag && )
	{
		std::cout << "SomeClassWithTag: move" << std::endl;
	}
};

struct SomeSomeClassWithTag
	: public SomeClassWithTag
{
	explicit SomeSomeClassWithTag( int _v )
		: SomeClassWithTag( _v )
	{
	}

	SomeSomeClassWithTag( const SomeSomeClassWithTag & _other )
		: SomeClassWithTag( _other )
	{
		std::cout << "SomeSomeClassWithTag: copy" << std::endl;
	}

	SomeSomeClassWithTag( SomeSomeClassWithTag && _other )
		: SomeClassWithTag( std::move( _other ) )
	{
		std::cout << "SomeSomeClassWithTag: move" << std::endl;
	}
};

DECLARE_TEST_CASE( UniversalReferenceOverloading_Solution_UseTags )
{
	std::string m1{};
	int m2{};
	short m3{};

	SomeClassWithTag s1{ m1 };
	SomeClassWithTag s2{ std::string( "some" ) };
	SomeClassWithTag s3{ "some" };
	SomeClassWithTag s4{ m2 }; // true_type
	SomeClassWithTag s5{ 10 }; // true_type
	SomeClassWithTag s6{ m3 }; // true_type (in opposite to case 'Problem')
}

DECLARE_TEST_CASE( UniversalReferenceOverloading_Solution_UseTags_Inheritance )
{
	SomeSomeClassWithTag s1{ 10 };
	const SomeSomeClassWithTag s2{ 10 };

	SomeSomeClassWithTag s3{ s1 }; // actually '&&', but logically expected 'copy'
	SomeSomeClassWithTag s4{ s2 }; // actually '&&', but logically expected 'copy'
}

//------------------------------------------------------------------------------

struct SomeClassSFINAE
{
	template<
			typename T
		,	typename = std::enable_if_t<
					!std::is_convertible_v<
							std::decay_t< T >
						,	SomeClassSFINAE
					>
				&&	!std::is_integral_v< std::decay_t< T > >
			>
	>
	explicit SomeClassSFINAE( T && )
	{
		std::cout << "SomeClassSFINAE: &&" << std::endl;
	}

	explicit SomeClassSFINAE( int )
	{
		std::cout << "SomeClassSFINAE: int" << std::endl;
	}

	SomeClassSFINAE( const SomeClassSFINAE & )
	{
		std::cout << "SomeClassSFINAE: copy" << std::endl;
	}

	SomeClassSFINAE( SomeClassSFINAE && )
	{
		std::cout << "SomeClassSFINAE: move" << std::endl;
	}
};

struct SomeSomeClassSFINAE
	: public SomeClassSFINAE
{
	explicit SomeSomeClassSFINAE( int _v )
		:	SomeClassSFINAE( _v )
	{
	}

	SomeSomeClassSFINAE( const SomeSomeClassSFINAE & _other )
		:	SomeClassSFINAE( _other )
	{
		std::cout << "SomeSomeClassSFINAE: copy" << std::endl;
	}

	SomeSomeClassSFINAE( SomeSomeClassSFINAE && _other )
		:	SomeClassSFINAE( std::move( _other ) )
	{
		std::cout << "SomeSomeClassSFINAE: move" << std::endl;
	}
};

DECLARE_TEST_CASE( UniversalReferenceOverloading_Solution_UseSFINAE )
{
	std::string m1{};
	int m2{};
	short m3{};

	SomeClassSFINAE s1{ m1 };
	SomeClassSFINAE s2{ std::string( "some" ) };
	SomeClassSFINAE s3{ "some" };
	SomeClassSFINAE s4{ m2 };
	SomeClassSFINAE s5{ 10 };
	SomeClassSFINAE s6{ m3 }; // int (in opposite to case 'Problem')
}

DECLARE_TEST_CASE( UniversalReferenceOverloading_Solution_UseSFINAE_Inheritance )
{
	SomeSomeClassSFINAE s1{ 10 };
	const SomeSomeClassSFINAE s2{ 10 };

	SomeSomeClassSFINAE s3{ s1 }; // copy (in opposite to case 'Problem' and 'Use Tags'
	SomeSomeClassSFINAE s4{ s2 }; // copy (in opposite to case 'Problem' and 'Use Tags'
}

//------------------------------------------------------------------------------
