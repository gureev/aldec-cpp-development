#ifndef __META_POW_HPP__
#define __META_POW_HPP__

//------------------------------------------------------------------------------

namespace Meta {

//------------------------------------------------------------------------------

template< int _N, int _M >
struct Pow
{
	static const int value = _N * Pow< _N, _M - 1 >::value;
};

template< int _N >
struct Pow< _N, 0 >
{
	static const int value = 1;
};

//------------------------------------------------------------------------------

} // namespace Meta

//------------------------------------------------------------------------------

#endif // __META_POW_HPP__
