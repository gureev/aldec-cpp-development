#ifndef __META_POLICY_CLONE_DUPLICATOR_HPP__
#define __META_POLICY_CLONE_DUPLICATOR_HPP__

//------------------------------------------------------------------------------

namespace Meta::Policy {

//------------------------------------------------------------------------------

template< typename T >
struct CloneDuplicator
{
	static std::unique_ptr< T > Clone( T & _t )
	{
		return _t.Clone();
	}
};

//------------------------------------------------------------------------------

} // namespace Meta::Policy

//------------------------------------------------------------------------------

#endif // __META_POLICY_CLONE_DUPLICATOR_HPP__
