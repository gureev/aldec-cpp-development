#ifndef __META_POLICY_DEFAULT_DESTRUCTOR_HPP__
#define __META_POLICY_DEFAULT_DESTRUCTOR_HPP__

//------------------------------------------------------------------------------

namespace Meta::Policy {

//------------------------------------------------------------------------------

template< typename T >
struct DefaultDestructor
{
	static void Destruct( T * /*_t*/ )
	{
	}
};

//------------------------------------------------------------------------------

} // namespace Meta::Policy

//------------------------------------------------------------------------------

#endif // __META_POLICY_DEFAULT_DESTRUCTOR_HPP__
