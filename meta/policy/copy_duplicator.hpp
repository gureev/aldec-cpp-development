#ifndef __META_POLICY_COPY_DUPLICATOR_HPP__
#define __META_POLICY_COPY_DUPLICATOR_HPP__

//------------------------------------------------------------------------------

namespace Meta::Policy {

//------------------------------------------------------------------------------

template< typename T >
struct CopyDuplicator
{
	static std::unique_ptr< T > Clone( T & _t )
	{
		return _t.Copy();
	}
};

//------------------------------------------------------------------------------

} // namespace Meta::Policy

  //------------------------------------------------------------------------------

#endif // __META_POLICY_COPY_DUPLICATOR_HPP__
