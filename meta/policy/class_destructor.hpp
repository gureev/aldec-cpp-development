#ifndef __META_POLICY_CLASS_DESTRUCTOR_HPP__
#define __META_POLICY_CLASS_DESTRUCTOR_HPP__

//------------------------------------------------------------------------------

namespace Meta::Policy {

//------------------------------------------------------------------------------

template< typename T >
struct ClassDestructor
{
	static void Destruct( T * _t )
	{
		_t->~T();
	}
};

//------------------------------------------------------------------------------

} // namespace Meta::Policy

//------------------------------------------------------------------------------

#endif // __META_POLICY_CLASS_DESTRUCTOR_HPP__
