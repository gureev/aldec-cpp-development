#ifndef __META_POLICY_NEW_DUPLICATOR_HPP__
#define __META_POLICY_NEW_DUPLICATOR_HPP__

//------------------------------------------------------------------------------

namespace Meta::Policy {

//------------------------------------------------------------------------------

template< typename T >
struct NewDuplicator
{
	static std::unique_ptr< T > Clone( T & _t )
	{
		return std::unique_ptr< T >( new T( _t ) );
	}
};

//------------------------------------------------------------------------------

} // namespace Meta::Policy

//------------------------------------------------------------------------------

#endif // __META_POLICY_NEW_DUPLICATOR_HPP__
