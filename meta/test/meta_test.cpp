#include "ph/ph.hpp"

#include "meta/is_class.hpp"
#include "meta/is_same.hpp"
#include "meta/is_convertible.hpp"
#include "meta/is_noexcept_copy.hpp"
#include "meta/pow.hpp"
#include "meta/has_clone_method.hpp"
#include "meta/has_copy_method.hpp"

#include "meta/policy/new_duplicator.hpp"
#include "meta/policy/clone_duplicator.hpp"
#include "meta/policy/copy_duplicator.hpp"

#include "meta/algorithm/duplicator_as_method.hpp"
#include "meta/algorithm/duplicator_as_template.hpp"
#include "meta/algorithm/duplicator_as_implicit.hpp"

#include "meta/enum/enum_switch.hpp"

#include "meta/sfinae/sfinae_class_template.hpp"
#include "meta/sfinae/sfinae_return_type.hpp"
#include "meta/sfinae/sfinae_method_template.hpp"
#include "meta/sfinae/sfinae_default_parameter.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

namespace Meta::Test {

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_IsClass )
{
	class SomeClass
	{
	};
	struct SomeStruct
	{
	};

	ASSERT( Meta::IsClass< void >::value == false );
	ASSERT( Meta::IsClass< int >::value == false );

	ASSERT( Meta::IsClass< SomeClass >::value == true );
	ASSERT( Meta::IsClass< SomeStruct >::value == true );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_IsSame )
{
	class SomeClass
	{
	};
	struct SomeStruct
	{
	};

	ASSERT( Meta::IsSame< void, int >::value == false );
	ASSERT( Meta::IsSame< int, int >::value == true );

	ASSERT( Meta::IsSame< SomeClass, SomeStruct >::value == false );
	ASSERT( Meta::IsSame< SomeClass, SomeClass >::value == true );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_IsConvertible )
{
	class SomeClass
	{
	};
	struct SomeStruct
		:	public SomeClass
	{
	};

	ASSERT( Meta::IsConvertible< void, int >::value == true );
	ASSERT( Meta::IsConvertible< int, int >::value == true );
	ASSERT( Meta::IsConvertible< double, int >::value == false );

	ASSERT( Meta::IsConvertible< SomeClass, SomeClass >::value == true );
	ASSERT( Meta::IsConvertible< SomeStruct, SomeClass >::value == false );
	ASSERT( Meta::IsConvertible< SomeClass, SomeStruct >::value == true );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_Pow )
{
	ASSERT( Meta::Pow< 2, 0 >::value == 1 );
	ASSERT( Meta::Pow< 2, 1 >::value == 2 );
	ASSERT( Meta::Pow< 2, 2 >::value == 4 );
	ASSERT( Meta::Pow< 2, 3 >::value == 8 );
	ASSERT( Meta::Pow< 2, 4 >::value == 16 );
	ASSERT( Meta::Pow< 2, 5 >::value == 32 );

	char a[ Meta::Pow< 2, 5 >::value ];
	ASSERT( sizeof( a ) == 32 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_HasCloneMethod )
{
	struct Func
	{
		void operator () ()
		{
		}
	};
	struct X1
	{
		int Clone;
	};
	struct X2
	{
		void Clone()
		{
		}
	};
	struct X3
	{
	};
	struct X4
	{
		Func Clone;
	};


	ASSERT( Meta::HasCloneMethod< X1 >::value == false );
	ASSERT( Meta::HasCloneMethod< X2 >::value == true );
	ASSERT( Meta::HasCloneMethod< X3 >::value == false );
	ASSERT( Meta::HasCloneMethod< X4 >::value == false );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_HasCopyMethod )
{
	struct Func
	{
		void operator () ()
		{
		}
	};
	struct X1
	{
		int Copy;
	};
	struct X2
	{
		void Copy()
		{
		}
	};
	struct X3
	{
	};
	struct X4
	{
		Func Copy;
	};


	ASSERT( Meta::HasCopyMethod< X1 >::value == false );
	ASSERT( Meta::HasCopyMethod< X2 >::value == true );
	ASSERT( Meta::HasCopyMethod< X3 >::value == false );
	ASSERT( Meta::HasCopyMethod< X4 >::value == false );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_Policy_NewDuplicator )
{
	struct A
	{
		int m_v{ 0 };
	};

	A source{ 123 };
	std::unique_ptr< A > target = Meta::Policy::NewDuplicator< A >::Clone( source );

	ASSERT( source.m_v == 123 );
	ASSERT( target->m_v == 123 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_Policy_CloneDuplicator )
{
	struct A
	{
		std::unique_ptr< A > Clone()
		{
			auto ptr = std::make_unique< A >( *this );
			ptr->m_v = 1;
			return ptr;
		}
		std::unique_ptr< A > Copy()
		{
			auto ptr = std::make_unique< A >( *this );
			ptr->m_v = 2;
			return ptr;
		}

		int m_v{ 0 };
	};

	A source{ 0 };
	std::unique_ptr< A > target = Meta::Policy::CloneDuplicator< A >::Clone( source );

	ASSERT( source.m_v == 0 );
	ASSERT( target->m_v == 1 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_Policy_CopyDuplicator )
{
	struct A
	{
		std::unique_ptr< A > Clone()
		{
			auto ptr = std::make_unique< A >( *this );
			ptr->m_v = 1;
			return ptr;
		}
		std::unique_ptr< A > Copy()
		{
			auto ptr = std::make_unique< A >( *this );
			ptr->m_v = 2;
			return ptr;
		}

		int m_v{ 0 };
	};

	A source{ 0 };
	std::unique_ptr< A > target = Meta::Policy::CopyDuplicator< A >::Clone( source );

	ASSERT( source.m_v == 0 );
	ASSERT( target->m_v == 2 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_SFINAE_ClassTemplate )
{
	SFINAE::ClassTemplate< int > ts1;
	SFINAE::ClassTemplate< double > ts2;

	ASSERT( ts1.action() == 20 );
	ASSERT( ts2.action() == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_Algorithm_DuplicatorAsMethod )
{
	struct A
	{
		std::unique_ptr< A > Clone()
		{
			auto ptr = std::make_unique< A >( *this );
			ptr->m_v = 1;
			return ptr;
		}

		int m_v{ 0 };
	};
	struct B
	{
		int m_v{ 3 };
	};
	struct C
	{
		std::unique_ptr< C > Copy()
		{
			auto ptr = std::make_unique< C >( *this );
			ptr->m_v = 1;
			return ptr;
		}

		int m_v{ 0 };
	};

	A a;
	B b;
	C c;

	std::unique_ptr< A > pA = Algorithm::DuplicatorAsMethod().Clone( a );
	std::unique_ptr< B > pB = Algorithm::DuplicatorAsMethod().Clone( b );
	std::unique_ptr< C > pC = Algorithm::DuplicatorAsMethod().Clone( c );

	ASSERT( a.m_v == 0 );
	ASSERT( pA->m_v == 1 );

	ASSERT( b.m_v == 3 );
	ASSERT( pB->m_v == 3 );

	ASSERT( c.m_v == 0 );
	ASSERT( pC->m_v == 1 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_Algorithm_DuplicatorAsTemplate )
{
	struct A
	{
		std::unique_ptr< A > Clone()
		{
			auto ptr = std::make_unique< A >( *this );
			ptr->m_v = 1;
			return ptr;
		}

		int m_v{ 0 };
	};
	struct B
	{
		int m_v{ 3 };
	};
	struct C
	{
		std::unique_ptr< C > Copy()
		{
			auto ptr = std::make_unique< C >( *this );
			ptr->m_v = 1;
			return ptr;
		}

		int m_v{ 0 };
	};

	A a;
	B b;
	C c;

	std::unique_ptr< A > pA = Algorithm::DuplicatorAsTemplate< Policy::CloneDuplicator >().Clone( a );
	std::unique_ptr< B > pB = Algorithm::DuplicatorAsTemplate< Policy::NewDuplicator >().Clone( b );
	std::unique_ptr< C > pC = Algorithm::DuplicatorAsTemplate< Policy::CopyDuplicator >().Clone( c );

	ASSERT( a.m_v == 0 );
	ASSERT( pA->m_v == 1 );

	ASSERT( b.m_v == 3 );
	ASSERT( pB->m_v == 3 );

	ASSERT( c.m_v == 0 );
	ASSERT( pC->m_v == 1 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_Algorithm_DuplicatorAsImplicit )
{
	struct A
	{
		std::unique_ptr< A > Clone()
		{
			auto ptr = std::make_unique< A >( *this );
			ptr->m_v = 1;
			return ptr;
		}

		int m_v{ 0 };
	};
	struct B
	{
		int m_v{ 3 };
	};
	struct C
	{
		std::unique_ptr< C > Copy()
		{
			auto ptr = std::make_unique< C >( *this );
			ptr->m_v = 1;
			return ptr;
		}

		int m_v{ 0 };
	};

	A a;
	B b;
	C c;

	std::unique_ptr< A > pA = Algorithm::DuplicatorAsImplicit().Clone( a );
	std::unique_ptr< B > pB = Algorithm::DuplicatorAsImplicit().Clone( b );
	std::unique_ptr< C > pC = Algorithm::DuplicatorAsImplicit().Clone( c );

	ASSERT( a.m_v == 0 );
	ASSERT( pA->m_v == 1 );

	ASSERT( b.m_v == 3 );
	ASSERT( pB->m_v == 3 );

	ASSERT( c.m_v == 0 );
	ASSERT( pC->m_v == 1 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_Algorithm_DuplicatorAsMethod_Conflict )
{
	struct A
	{
		std::unique_ptr< A > Clone()
		{
			auto ptr = std::make_unique< A >( *this );
			ptr->m_v = 1;
			return ptr;
		}
		/*std::unique_ptr< A > Copy()
		{
			auto ptr = std::make_unique< A >( *this );
			ptr->m_v = 2;
			return ptr;
		}*/

		int m_v{ 0 };
	};

	A a;

	std::unique_ptr< A > pA = Algorithm::DuplicatorAsMethod().Clone( a );

	ASSERT( a.m_v == 0 );
	ASSERT( pA->m_v == 1 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_Algorithm_DuplicatorAsTemplate_Conflict )
{
	struct A
	{
		std::unique_ptr< A > Clone()
		{
			auto ptr = std::make_unique< A >( *this );
			ptr->m_v = 1;
			return ptr;
		}
		std::unique_ptr< A > Copy()
		{
			auto ptr = std::make_unique< A >( *this );
			ptr->m_v = 2;
			return ptr;
		}

		int m_v{ 0 };
	};

	A a;

	std::unique_ptr< A > pA1 = Algorithm::DuplicatorAsTemplate< Policy::CloneDuplicator >().Clone( a );
	std::unique_ptr< A > pA2 = Algorithm::DuplicatorAsTemplate< Policy::CopyDuplicator >().Clone( a );
	std::unique_ptr< A > pA3 = Algorithm::DuplicatorAsTemplate< Policy::NewDuplicator >().Clone( a );

	ASSERT( a.m_v == 0 );
	ASSERT( pA1->m_v == 1 );
	ASSERT( pA2->m_v == 2 );
	ASSERT( pA3->m_v == 0 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_Algorithm_DuplicatorAsImplicit_Conflict )
{
	struct A
	{
		std::unique_ptr< A > Clone()
		{
			auto ptr = std::make_unique< A >( *this );
			ptr->m_v = 1;
			return ptr;
		}
		std::unique_ptr< A > Copy()
		{
			auto ptr = std::make_unique< A >( *this );
			ptr->m_v = 2;
			return ptr;
		}

		int m_v{ 0 };
	};

	A a;

	std::unique_ptr< A > pA = Algorithm::DuplicatorAsImplicit().Clone( a );

	ASSERT( a.m_v == 0 );
	ASSERT( pA->m_v == 1 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_SFINAE_ReturnType )
{
	SFINAE::ReturnType rt;

	ASSERT( rt.action( 0 ) == 20 );
	ASSERT( rt.action( 0.0 ) == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_SFINAE_MethodTemplate )
{
	SFINAE::MethodTemplate mt;

	ASSERT( mt.action( 0 ) == 20 );
	ASSERT( mt.action( 0.0 ) == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_SFINAE_DefaultParameter )
{
	SFINAE::DefaultParameter dp;

	ASSERT( dp.action( 0 ) == 20 );
	ASSERT( dp.action( 0.0 ) == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_EnumSwitch )
{
	enum class E1
	{
		L1, L2,
		Total
	};
	enum class E2
	{
		L1, L2,
		Count
	};
	enum class E3
	{
		L1, L2,
		L3
	};

	E1 e1 = E1::L1;
	E2 e2 = E2::L1;
	[[ maybe_unused ]] E3 e3 = E3::L1;

	ENUM_SWITCH( e1, 2 )
	{
		case E1::L1:
		case E1::L2:
		default:
			break;
	}

	ENUM_SWITCH( e2, 2 )
	{
		case E2::L1:
		case E2::L2:
		default:
			break;
	}

	// error C2338 : Add Count or Total literal to this enum!
	// error C2338 : Enum literals were changed, check this switch statement!
	/*
	ENUM_SWITCH( e3, 2 )
	{
		case E3::L1:
		case E3::L2:
		default:
			break;
	}
	*/
}

//------------------------------------------------------------------------------

template< typename _T > _T testV( _T );
template< typename _T > _T testLV( _T & );
template< typename _T > _T testUV( _T && );

DECLARE_TEST_CASE( MetaTest_FunctionTemplateTypeDeduction )
{
	int i{ 0 };
	const int ci = i;
	const int & cr{ 0 };
	int a[] = { 1, 2, 3, 4, 5 };

	ASSERT( Meta::IsSame< decltype( testV( i ) ), int >::value );
	ASSERT( Meta::IsSame< decltype( testV( ci ) ), int >::value );
	ASSERT( Meta::IsSame< decltype( testV( cr ) ), int >::value );
	ASSERT( Meta::IsSame< decltype( testV( 10 ) ), int >::value );
	ASSERT( Meta::IsSame< decltype( testV( a ) ), int * >::value );

	ASSERT( Meta::IsSame< decltype( testLV( i ) ), int >::value );
	//ASSERT( Meta::IsSame< decltype( testLV( ci ) ), const int >::value );
	//ASSERT( Meta::IsSame< decltype( testLV( cr ) ), const int >::value );
	ASSERT( Meta::IsSame< decltype( testLV( ci ) ), int >::value ); // because of decltype( prvalue )
	ASSERT( Meta::IsSame< decltype( testLV( cr ) ), int >::value ); // because of decltype( prvalue )

	/*
		decltype( identifier )
			exact type. identifier is variable name e.x.
		decltype( other expression )
			if expression xvalue > type &&
			if expression lvalue > type &
			if expression prvalue > type
	*/

	ASSERT( Meta::IsSame< decltype( testUV( i ) ), int & >::value );
	ASSERT( Meta::IsSame< decltype( testUV( ci ) ), const int & >::value );
	ASSERT( Meta::IsSame< decltype( testUV( cr ) ), const int & >::value );
	ASSERT( Meta::IsSame< decltype( testUV( 10 ) ), int >::value );
	ASSERT( Meta::IsSame< decltype( testUV( a ) ), int (&)[5] >::value );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_AutoTypeDeduction )
{
	int i{ 0 };
	const int ci{ 0 };
	const int & cr{ 0 };
	int a[] = { 1, 2, 3, 4, 5 };

	auto v1 = i;
	ASSERT( Meta::IsSame< decltype( v1 ), int >::value );

	auto v2 = ci;
	ASSERT( Meta::IsSame< decltype( v2 ), int >::value );

	auto v3 = cr;
	ASSERT( Meta::IsSame< decltype( v3 ), int >::value );

	auto v4 = 10;
	ASSERT( Meta::IsSame< decltype( v4 ), int >::value );

	auto v5 = a;
	ASSERT( Meta::IsSame< decltype( v5 ), int * >::value );

	auto v6( 10 );
	ASSERT( Meta::IsSame< decltype( v6 ), int >::value );

	auto v7{ 10 };
	ASSERT( Meta::IsSame< decltype( v7 ), int >::value );

	auto v8 = { 10 };
	ASSERT( Meta::IsSame< decltype( v8 ), std::initializer_list< int > >::value );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_DecltypeTypeDeduction )
{
	int i{ 0 };
	const int ci{ 0 };
	const int & cr{ 0 };
	int a[] = { 1, 2, 3, 4, 5 };

	decltype( i ) v1 = i;
	ASSERT( Meta::IsSame< decltype( v1 ), int >::value );

	decltype( ci ) v2 = ci;
	ASSERT( Meta::IsSame< decltype( v2 ), const int >::value );

	decltype( cr ) v3 = cr;
	ASSERT( Meta::IsSame< decltype( v3 ), const int & >::value );

	decltype( 10 ) v4 = 10;
	ASSERT( Meta::IsSame< decltype( v4 ), int >::value );

	decltype( a ) v5;
	ASSERT( Meta::IsSame< decltype( v5 ), int [5] >::value );

	decltype( ( i ) ) v6 = i;
	ASSERT( Meta::IsSame< decltype( v6 ), int & >::value );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_DecltypeAutoTypeDeduction )
{
	int i{ 0 };
	const int ci{ 0 };
	const int & cr{ 0 };
	int a[] = { 1, 2, 3, 4, 5 };

	decltype(auto) v1 = i;
	ASSERT( Meta::IsSame< decltype( v1 ), int >::value );

	decltype( auto ) v2 = ci;
	ASSERT( Meta::IsSame< decltype( v2 ), const int >::value );

	decltype( auto ) v3 = cr;
	ASSERT( Meta::IsSame< decltype( v3 ), const int & >::value );

	decltype( auto ) v4 = 10;
	ASSERT( Meta::IsSame< decltype( v4 ), int >::value );

	//decltype( auto ) v5 = a; // array initialization requires a brace-enclosed initializer list
	//ASSERT( Meta::IsSame< decltype( v5 ), int * >::value );

	//decltype( auto ) v6{ 10 }; // cannot deduce a type as an initializer list is not an expression
	//ASSERT( Meta::IsSame< decltype( v6 ), int >::value );

	//decltype( auto ) v7 = { 10 }; // cannot deduce a type as an initializer list is not an expression
	//ASSERT( Meta::IsSame< decltype( v7 ), std::initializer_list< int > >::value );
}

//------------------------------------------------------------------------------

template< typename T >
class TD;

template< typename T >
struct MyS
{
	decltype( std::declval< T && >() + 1 ) m_v;
};

DECLARE_TEST_CASE( MetaTest_TypeDeduceDebug )
{
	MyS< int > s;
	ASSERT( Meta::IsSame< decltype( s.m_v ), int >::value );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_IsNoexceptCopy_Nothing )
{
	class SomeClass
	{
	public:
		SomeClass() {}
		SomeClass( const SomeClass & ) {}
		SomeClass( SomeClass && ) {}
	};

	ASSERT( Meta::IsNoexeptCopy< SomeClass >::value == false );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_IsNoexceptCopy_DefaultConstructor )
{
	class SomeClass
	{
	public:
		SomeClass() noexcept {}
		SomeClass( const SomeClass & ) {}
		SomeClass( SomeClass && ) {}
	};

	ASSERT( Meta::IsNoexeptCopy< SomeClass >::value == false );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_IsNoexceptCopy_CopyConstructor )
{
	class SomeClass
	{
	public:
		SomeClass() {}
		SomeClass( const SomeClass & ) noexcept {}
		SomeClass( SomeClass && ) {}
	};

	ASSERT( Meta::IsNoexeptCopy< SomeClass >::value == true );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_IsNoexceptCopy_MoveConstructor )
{
	class SomeClass
	{
	public:
		SomeClass() {}
		SomeClass( const SomeClass & ) {}
		SomeClass( SomeClass && ) noexcept {}
	};

	ASSERT( Meta::IsNoexeptCopy< SomeClass >::value == false );
}

//------------------------------------------------------------------------------

template< typename T >
struct DotTemplateX
{
	template< typename U >
	int execute()
	{
		return 5;
	}
};

template< typename T >
int DotTemplateY()
{
	DotTemplateX< T > x;
	return x.execute< T >(); // MSVS is smart to avoid 'template'
}

DECLARE_TEST_CASE( MetaTest_DotTemplate )
{
	auto r = DotTemplateY< int >();
	ASSERT( r == 5 );
}

//------------------------------------------------------------------------------

namespace DotTemplateTypename
{
	template< typename T >
	struct DependentType
	{
		using Type = int;
	};

	template< typename T >
	struct Base
	{
		template< typename U >
		int someBase()
		{
			return 5;
		}
	};

	template< typename T >
	struct Derived
		:	public Base< T >
	{
		int someDerived()
		{
			// return someBase< DependentType< T >::Type >();
			return this->template someBase< typename DependentType< T >::Type >();
		}
	};
}

DECLARE_TEST_CASE( MetaTest_DotTemplateTypename )
{
	DotTemplateTypename::Derived< int > d;
	ASSERT( d.someDerived() == 5 );
}

//------------------------------------------------------------------------------

template< typename T >
struct BaseThisX
{
	int exit()
	{
		return 123;
	}
};

template< typename T >
struct BaseThisY
	:	public BaseThisX< T >
{
	int f()
	{
		//return exit(); // error C2660: 'exit': function does not take 0 arguments
		return this->exit();
	}
};

DECLARE_TEST_CASE( MetaTest_BaseThisMethod )
{
	BaseThisY< int > y;
	ASSERT( y.f() == 123 );
}

//------------------------------------------------------------------------------

template< typename T, template< typename > typename C >
struct XT
{
	C< T > m_c;
};

template< typename T >
using myVector = std::vector< T >;

template< template< typename, template< typename R > typename > typename T >
struct XXT
{
	T< int, myVector > m_c;
};

DECLARE_TEST_CASE( MetaTest_TemplateTemplateParameter )
{
	XT< int, std::vector > xt1;
	xt1.m_c.emplace_back();
	ASSERT( xt1.m_c.size() == 1 );

	XT< int, myVector > xt2;
	xt2.m_c.emplace_back();
	ASSERT( xt2.m_c.size() == 1 );

	XXT< XT > xxt;
	xxt.m_c.m_c.emplace_back();
	ASSERT( xxt.m_c.m_c.size() == 1 );
}

//------------------------------------------------------------------------------

struct error_novel_X
{
	using Pointer = int;
};

template< typename T >
void error_novel_level_1( T & _t )
{
	* _t = 5;
}
template< typename T >
void error_novel_level_2( T & _t )
{
	error_novel_level_1( _t );
}
template< typename T >
void error_novel_level_3( typename T::Pointer & _t )
{
	error_novel_level_2( _t );
}
template< typename T >
void error_novel_level_4( T & _t )
{
	typename T::Pointer ptr;
	error_novel_level_3< T >( ptr );
}
template< typename T >
void error_novel_level_5()
{
	T x;
	error_novel_level_4( x );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_ErrorNovel_1 )
{
	std::list< std::string > l;
	auto it = std::find_if(
			l.begin()
		,	l.end()
		,	std::bind( std::greater< /*int*/ std::string >(), "kill me", std::placeholders::_1 )
	);
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MetaTest_ErrorNovel_2 )
{
	//error_novel_level_5< error_novel_X >();
}

//------------------------------------------------------------------------------

struct StaticX
{
	static int x;
};
int StaticX::x{ 10 };

template< typename T, T _value >
struct ModifierX
{
	void exec()
	{
		T & ref = _value;
		ref = 15;
	}
};

DECLARE_TEST_CASE( MetaTest_StaticTemplateModification )
{
	ASSERT( StaticX::x == 10 );
	ModifierX< int &, StaticX::x >().exec();
	ASSERT( StaticX::x == 15 );
}

//------------------------------------------------------------------------------

template< typename T >
struct X1
{
	template< typename U >
	struct X2
	{
		template< typename R >
		struct X3
		{
			void f()
			{
			}
		};
	};
};

template< typename T, typename U, typename R >
struct XA
{
	void f( typename X1< T >::template X2< U >::template X3< R > _x )
	{
		_x.f();
	}
};

DECLARE_TEST_CASE( MetaTest_DeepDependentTemplateName )
{
	auto x = X1< int >::X2< short >::X3< int >{};
	x.f();

	XA< int, short, int > xa{};
	xa.f( x );
}

//------------------------------------------------------------------------------

template< typename T >
void fullDependentName( T _t )
{
	// Function-call with non-full dependent names. Search only with OL at first stage
	fullDependentNameNext( _t );
}
void fullDependentNameNext( int )
{
}

DECLARE_TEST_CASE( MetaTest_POI_FullDependentName )
{
	// NOTE: MSVC compiles this, but it's not allowed by standard
	fullDependentName( 10 ); // No additional scopes for built-in types
}

//------------------------------------------------------------------------------

struct FullDependentNameX
{
};
void fullDependentNameNext( FullDependentNameX )
{
}

DECLARE_TEST_CASE( MetaTest_POI_FullDependentName_FoundByADL )
{
	fullDependentName( FullDependentNameX{} );
}

//------------------------------------------------------------------------------

namespace TemplateUpcast
{
	template< typename T >
	struct Base
	{
	};
	template< typename T >
	struct Derived : public Base< T >
	{
	};

	template< typename T >
	void f( Base< T > & )
	{
	}
}

DECLARE_TEST_CASE( MetaTest_TemplateUpcast )
{
	TemplateUpcast::Derived< int > d;
	f( d ); // f< int >
}

//------------------------------------------------------------------------------

namespace TemplateOverloadsProblems
{
	template< typename T >
	int f( T )
	{
		return 5;
	}
	template< typename T >
	int f( T * )
	{
		return 10;
	}
}

DECLARE_TEST_CASE( MetaTest_TemplateOverloadsProblems )
{
	int * pInt = nullptr;
	ASSERT( TemplateOverloadsProblems::f< int * >( pInt ) == 5 );
	ASSERT( TemplateOverloadsProblems::f< int >( pInt ) == 10 );
}

//------------------------------------------------------------------------------

namespace TypeTraitsAccumulate
{
	template< typename T >
	struct AccumulateTraits
	{
		using Type = T;
		static constexpr Type zero()
		{
			return {};
		}
	};
	template<>
	struct AccumulateTraits< char >
	{
		using Type = int;
		static constexpr Type zero()
		{
			return {};
		}
	};
	struct SumPolicy
	{
		template< typename T, typename U >
		static void exec( T & _result, const U & _value )
		{
			_result += _value;
		}
	};

	template<
			typename T
		,	typename Policy = SumPolicy
		,	typename Traits = AccumulateTraits< T >
	>
	struct Accumulate
	{
		template< typename U >
		static auto exec( U _begin, U _end )
		{
			using Type = typename Traits::Type;
			Type r{ Traits::zero() };

			while ( _begin != _end )
			{
				Policy::exec( r, * _begin );
				++_begin;
			}
			return r;
		}
	};

	DECLARE_TEST_CASE( MetaTest_ComplexTypeTraitsExample )
	{
		using Type = char;
		std::array< Type, 5 > a{ '1', '2', '3', '4', '5' };
		auto r = Accumulate< Type >::exec( a.begin(), a.end() );
		ASSERT( ( r / 5 ) == '3' );
	}
}

//------------------------------------------------------------------------------

template< int N >
struct Transform
{
	template< typename T >
	static T exec( const T * _left, const T * _right )
	{
		return *_left + *_right + Transform< N - 1 >::exec( _left + 1, _right + 1 );
	}
};
template<>
struct Transform< 0 >
{
	template< typename T >
	static T exec( const T *, const T * )
	{
		return {};
	}
};

DECLARE_TEST_CASE( MetaTest_LoopExpansion )
{
	int left[] = { 1, 2, 3 };
	int right[] = { 4, 5, 6 };
	int result = Transform< 3 >::exec( left, right );

	ASSERT( result == ( 5 + 7 + 9 ) );
}

//------------------------------------------------------------------------------

template< typename T >
struct Deduction
{
	T t;
};

DECLARE_TEST_CASE( MetaTest_ClassTemplateDeduction )
{
	//Deduction d{ 10 };
}

//------------------------------------------------------------------------------

namespace FoldExpressions
{
	template< typename _Arg >
	void forItem( _Arg && _arg )
	{
		std::cout << _arg << std::endl;
	}

	template< typename ... _Args >
	void unary( _Args && ... _args )
	{
		( forItem( _args ), ... );
	}

	template< typename ... _Args >
	auto binary( _Args && ... _args )
	{
		return ( _args + ... + 0 );
	}
}

DECLARE_TEST_CASE( MetaTest_FoldExpressions_UnaryFold )
{
	FoldExpressions::unary( 1, 2, 3 );
}

DECLARE_TEST_CASE( MetaTest_FoldExpressions_BinaryFold )
{
	auto r = FoldExpressions::binary( 1, 2, 3 );
	ASSERT( r == 6 );
}

//------------------------------------------------------------------------------

namespace FoldExpressionsBefore17
{
	template< typename T >
	void printer( T && _t )
	{
		std::cout << _t << ' ';
	}

	template< typename ... T >
	void actionWrapper( T && ... )
	{
	}

	template< typename ... T >
	void execute( T && ... _t )
	{
		// compiler-specific order
		actionWrapper( ( printer( _t ), 0 )... );

		// strict order
		[[ maybe_unused ]] int dummy[] = { ( printer( _t ), 0 )... };

		for ( auto && v : { _t ... } )
			printer( v );

		std::cout << std::endl;
	}
}

DECLARE_TEST_CASE( MetaTest_FoldExpressions_Before17 )
{
	FoldExpressionsBefore17::execute( 1, 2, 3 );
}

//------------------------------------------------------------------------------

} // namespace Meta::Test
