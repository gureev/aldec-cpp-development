#ifndef __META_IS_CONVERTIBLE_HPP__
#define __META_IS_CONVERTIBLE_HPP__

//------------------------------------------------------------------------------

namespace Meta {

//------------------------------------------------------------------------------

template< typename _Base, typename _Derived >
class IsConvertible
{
	template< typename R > static std::true_type  run( R * );
	template< typename R > static std::false_type run( ... );

public:

	static const bool value = decltype( run< _Base >( std::declval< _Derived * >() ) )::value;
};

//------------------------------------------------------------------------------

} // namespace Meta

//------------------------------------------------------------------------------

#endif // __META_IS_CONVERTIBLE_HPP__
