#ifndef __META_IS_NOEXCEPT_COPY_HPP__
#define __META_IS_NOEXCEPT_COPY_HPP__

//------------------------------------------------------------------------------

namespace Meta {

//------------------------------------------------------------------------------

template< typename T >
class IsNoexeptCopy
{
	using result_t = std::conditional_t<
			noexcept( T( std::declval< T & >() ) )
		,	std::true_type
		,	std::false_type
	>;

public:

	static const bool value = result_t::value;
};

//------------------------------------------------------------------------------

} // namespace Meta

//------------------------------------------------------------------------------

#endif // __META_IS_NOEXCEPT_COPY_HPP__
