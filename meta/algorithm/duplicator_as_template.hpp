#ifndef __META_ALGORITHM_DUPLICATOR_AS_TEMPLATE_HPP__
#define __META_ALGORITHM_DUPLICATOR_AS_TEMPLATE_HPP__

//------------------------------------------------------------------------------

#include "meta/has_clone_method.hpp"
#include "meta/has_copy_method.hpp"

#include "meta/policy/clone_duplicator.hpp"
#include "meta/policy/copy_duplicator.hpp"
#include "meta/policy/new_duplicator.hpp"

//------------------------------------------------------------------------------

namespace Meta::Algorithm {

//------------------------------------------------------------------------------

template< template < typename T > typename _ClonePolicy >
struct DuplicatorAsTemplate
{
	template< typename T >
	std::unique_ptr< T > Clone( T & _t )
	{
		return _ClonePolicy< T >::Clone( _t );
	}
};

//------------------------------------------------------------------------------

} // namespace Meta::Algorithm

//------------------------------------------------------------------------------

#endif // __META_ALGORITHM_DUPLICATOR_AS_TEMPLATE_HPP__
