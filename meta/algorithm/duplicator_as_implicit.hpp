#ifndef __META_ALGORITHM_DUPLICATOR_AS_IMPLICIT_HPP__
#define __META_ALGORITHM_DUPLICATOR_AS_IMPLICIT_HPP__

//------------------------------------------------------------------------------

#include "meta/has_clone_method.hpp"
#include "meta/has_copy_method.hpp"

#include "meta/policy/clone_duplicator.hpp"
#include "meta/policy/copy_duplicator.hpp"
#include "meta/policy/new_duplicator.hpp"

//------------------------------------------------------------------------------

namespace Meta::Algorithm {

//------------------------------------------------------------------------------

struct DuplicatorAsImplicit
{
	template< typename T >
	std::unique_ptr< T > Clone( T & _t )
	{
		using Duplicator = std::conditional_t<
				Meta::HasCloneMethod< T >::value
			,	Policy::CloneDuplicator< T >
			,	std::conditional_t<
						Meta::HasCopyMethod< T >::value
					,	Policy::CopyDuplicator< T >
					,	Policy::NewDuplicator< T >
				>
		>;
		return Duplicator::Clone( _t );
	}
};

//------------------------------------------------------------------------------

} // namespace Meta::Algorithm

//------------------------------------------------------------------------------

#endif // __META_ALGORITHM_DUPLICATOR_AS_IMPLICIT_HPP__
