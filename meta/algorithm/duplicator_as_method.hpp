#ifndef __META_ALGORITHM_DUPLICATOR_AS_METHOD_HPP__
#define __META_ALGORITHM_DUPLICATOR_AS_METHOD_HPP__

//------------------------------------------------------------------------------

#include "meta/has_clone_method.hpp"
#include "meta/has_copy_method.hpp"

#include "meta/policy/clone_duplicator.hpp"
#include "meta/policy/copy_duplicator.hpp"
#include "meta/policy/new_duplicator.hpp"

//------------------------------------------------------------------------------

namespace Meta::Algorithm {

//------------------------------------------------------------------------------

struct DuplicatorAsMethod
{
	template< typename T >
	auto Clone( T & _t )
		-> std::enable_if_t< Meta::HasCloneMethod< T >::value, std::unique_ptr< T > >
	{
		return Policy::CloneDuplicator< T >::Clone( _t );
	}

	template< typename T >
	auto Clone( T & _t )
		-> std::enable_if_t< Meta::HasCopyMethod< T >::value, std::unique_ptr< T > >
	{
		return Policy::CopyDuplicator< T >::Clone( _t );
	}

	template< typename T >
	auto Clone( T & _t )
		->	std::enable_if_t<
					!Meta::HasCloneMethod< T >::value && !Meta::HasCopyMethod< T >::value
				,	std::unique_ptr< T >
			>
	{
		return Policy::NewDuplicator< T >::Clone( _t );
	}
};

//------------------------------------------------------------------------------

} // namespace Meta::Algorithm

//------------------------------------------------------------------------------

#endif // __META_ALGORITHM_DUPLICATOR_AS_METHOD_HPP__
