#ifndef __META_SFINAE_METHOD_TEMPLATE_HPP__
#define __META_SFINAE_METHOD_TEMPLATE_HPP__

//------------------------------------------------------------------------------

namespace Meta::SFINAE {

//------------------------------------------------------------------------------

struct MethodTemplate
{
	template< typename R, std::enable_if_t< std::is_integral< R >::value, R * > = nullptr >
	R action( R _r )
	{
		return 20 + _r;
	}
	template< typename R, std::enable_if_t< !std::is_integral< R >::value, R * > = nullptr >
	R action( R _r )
	{
		return 10 + _r;
	}
};

//------------------------------------------------------------------------------

} // namespace Meta::SFINAE

//------------------------------------------------------------------------------

#endif // __META_SFINAE_METHOD_TEMPLATE_HPP__
