#ifndef __META_SFINAE_DEFAULT_PARAMETER_HPP__
#define __META_SFINAE_DEFAULT_PARAMETER_HPP__

//------------------------------------------------------------------------------

namespace Meta::SFINAE {

//------------------------------------------------------------------------------

struct DefaultParameter
{
	template< typename R >
	R action( R _r, std::enable_if_t< std::is_integral< R >::value, R * > = nullptr )
	{
		return 20 + _r;
	}
	template< typename R >
	R action( R _r, std::enable_if_t< !std::is_integral< R >::value, R * > = nullptr )
	{
		return 10 + _r;
	}
};

//------------------------------------------------------------------------------

} // namespace Meta::SFINAE

//------------------------------------------------------------------------------

#endif // __META_SFINAE_DEFAULT_PARAMETER_HPP__
