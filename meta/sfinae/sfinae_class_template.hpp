#ifndef __META_SFINAE_CLASS_TEMPLATE_HPP__
#define __META_SFINAE_CLASS_TEMPLATE_HPP__

//------------------------------------------------------------------------------

namespace Meta::SFINAE {

//------------------------------------------------------------------------------

template< typename T, typename = void >
struct ClassTemplate
{
	int action()
	{
		return 10;
	}
};

//------------------------------------------------------------------------------

template< typename T >
struct ClassTemplate<
		T
	,	std::enable_if_t<
			std::is_integral< T >::value
		>
>
{
	int action()
	{
		return 20;
	}
};

//------------------------------------------------------------------------------

} // namespace Meta::SFINAE

//------------------------------------------------------------------------------

#endif // __META_SFINAE_CLASS_TEMPLATE_HPP__
