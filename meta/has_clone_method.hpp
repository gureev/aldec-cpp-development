#ifndef __META_HAS_CLONE_METHOD_HPP__
#define __META_HAS_CLONE_METHOD_HPP__

//------------------------------------------------------------------------------

namespace Meta {

//------------------------------------------------------------------------------

template< typename T >
class HasCloneMethod
{
	template<
			typename R
		,	typename = std::enable_if_t<
				std::is_member_function_pointer< decltype( & R::Clone ) >::value
			>
	>
	static std::true_type run( R * );

	template< typename R >
	static std::false_type run( ... );

public:

	static const bool value = decltype( run< T >( nullptr ) )::value;
};

//------------------------------------------------------------------------------

} // namespace Meta

//------------------------------------------------------------------------------

#endif // __META_HAS_CLONE_METHOD_HPP__
