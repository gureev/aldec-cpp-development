#ifndef __META_ENUM_SWITCH_ENUM_HPP__
#define __META_ENUM_SWITCH_ENUM_HPP__

//------------------------------------------------------------------------------

namespace Meta::Enum {

//------------------------------------------------------------------------------

namespace Private {

//------------------------------------------------------------------------------

struct SizeMethod
{
	enum Enum
	{
			None = 1
		,	Count
		,	Total
	};
};

//------------------------------------------------------------------------------

template< typename T >
class SizeMethodExtractor
{
	class NoneMethod { char v[ SizeMethod::None ]; };
	class CountMethod { char v[ SizeMethod::Count ]; };
	class TotalMethod { char v[ SizeMethod::Total ]; };

	template< typename R > static NoneMethod run( ... );
	template< typename R > static CountMethod run( decltype( R::Count ) * );
	template< typename R > static TotalMethod run( decltype( R::Total ) * );

public:

	static const SizeMethod::Enum value = static_cast< SizeMethod::Enum >
		( sizeof( run< T >( nullptr  ) ) );
};

//------------------------------------------------------------------------------

template< typename T, SizeMethod::Enum _method >
struct EnumSize
{
	static const int value = 0;
};

template< typename T >
struct EnumSize< T, SizeMethod::Count >
{
	static const int value = static_cast< int >( T::Count );
};

template< typename T >
struct EnumSize< T, SizeMethod::Total >
{
	static const int value = static_cast< int >( T::Total );
};

//------------------------------------------------------------------------------

} // namespace Private

//------------------------------------------------------------------------------

#define ENUM_STATIC_ASSERT( _enumType, _itemsCount )										\
{																							\
	const Meta::Enum::Private::SizeMethod::Enum method =									\
		Meta::Enum::Private::SizeMethodExtractor< _enumType >::value;						\
																							\
	static_assert(																			\
			method != Meta::Enum::Private::SizeMethod::None									\
		,	"Add Count or Total literal to this enum!"										\
	);																						\
																							\
	static_assert(																			\
			( Meta::Enum::Private::EnumSize< _enumType, method >::value == _itemsCount )	\
		,	"Enum literals were changed, check this switch statement!"						\
	);																						\
}

//------------------------------------------------------------------------------

#define ENUM_SWITCH( _enumValue, _itemsCount )												\
ENUM_STATIC_ASSERT( decltype( _enumValue ), _itemsCount )									\
switch ( _enumValue )

//------------------------------------------------------------------------------

} // namespace Meta::Enum

//------------------------------------------------------------------------------

#endif // __META_ENUM_SWITCH_ENUM_HPP__
