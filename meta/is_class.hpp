#ifndef __META_IS_CLASS_HPP__
#define __META_IS_CLASS_HPP__

//------------------------------------------------------------------------------

namespace Meta {

//------------------------------------------------------------------------------

template< typename T >
class IsClass
{
	template< typename R > static std::true_type  run( int R::* );
	template< typename R > static std::false_type run( ... );

public:

	static const bool value = decltype( run< T >( nullptr ) )::value;
};

//------------------------------------------------------------------------------
	
} // namespace Meta

//------------------------------------------------------------------------------

#endif // __META_IS_CLASS_HPP__
