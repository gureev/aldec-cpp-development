#ifndef __META_IS_SAME_HPP__
#define __META_IS_SAME_HPP__

//------------------------------------------------------------------------------

namespace Meta {

//------------------------------------------------------------------------------

template< typename T1, typename T2 >
struct IsSame
	:	public std::false_type
{
};

template< typename T >
struct IsSame< T, T >
	:	public std::true_type
{
};

//------------------------------------------------------------------------------

} // namespace Meta

//------------------------------------------------------------------------------

#endif // __META_IS_SAME_HPP__
