#include "ph/ph.hpp"

#include "constexpr/pow.hpp"
#include "constexpr/strlen.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

namespace Constexpr::Test {

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ConstexprTest_Pow )
{
	ASSERT( Constexpr::pow( 2, 0 ) == 1 );
	ASSERT( Constexpr::pow( 2, 1 ) == 2 );
	ASSERT( Constexpr::pow( 2, 2 ) == 4 );
	ASSERT( Constexpr::pow( 2, 3 ) == 8 );
	ASSERT( Constexpr::pow( 2, 4 ) == 16 );
	ASSERT( Constexpr::pow( 2, 5 ) == 32 );

	char a[ Constexpr::pow( 2, 5 ) ];
	ASSERT( sizeof( a ) == 32 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ConstexprTest_Enum )
{
	struct ConfigurationFlags
	{
		enum Enum
		{
				Empty
			,	UseTop
			,	UseAny
		};

		static constexpr int toArraySize( Enum _enum )
		{
			switch ( _enum )
			{
				case Empty:		return 1;
				case UseTop:	return 5;
				case UseAny:	return 9;

				default:
					break;
			}

			assert( !"Unknown enum value" );
			return 0;
		}
	};

	char data[ ConfigurationFlags::toArraySize( ConfigurationFlags::UseTop ) ];
	ASSERT( sizeof( data ) == 5 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ConstexprTest_Strlen )
{
	struct ConfigurationFlags
	{
		enum Enum
		{
				Empty
			,	UseTop
			,	UseAny
		};

		static constexpr const char * toString( Enum _enum )
		{
			switch ( _enum )
			{
				case Empty:		return "Empty";
				case UseTop:	return "Top";
				case UseAny:	return "Any";

				default:
					break;
			}

			assert( !"Unknown enum value" );
			return nullptr;
		}
	};

	auto dataSize = []() constexpr
	{
		return Constexpr::strlen( ConfigurationFlags::toString( ConfigurationFlags::UseAny ) );
	};

	char data[ dataSize() ];
	ASSERT( sizeof( data ) == 3 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ConstexprTest_Struct )
{
	class Point
	{
	public:
		constexpr Point( int _x, int _y ) noexcept
			:	m_x( _x )
			,	m_y( _y )
		{
		}
		constexpr int getX() const noexcept
		{
			return m_x;
		}
		constexpr int getY() const noexcept
		{
			return m_y;
		}
		constexpr void setX( int _x ) noexcept // valid only since C++14
		{
			m_x = _x;
		}
		void someOtherMethod()
		{
		}
	private:
		int m_x;
		int m_y;
	};

	auto makePoint = []( Point _p1, Point _p2 ) constexpr noexcept -> Point
	{
		return {
				_p1.getX() + _p2.getX()
			,	_p1.getY() + _p2.getY()
		};
	};

	constexpr Point p1 = makePoint( Point{ 1, 1 }, Point{ 2, 2 } );
	std::array< int, p1.getX() > a1;
	ASSERT( a1.size() == 3 );

	auto changePoint = []( Point _p ) constexpr noexcept
	{
		Point copy( _p );
		copy.setX( 16 );
		return copy;
	};

	constexpr Point p2 = changePoint( p1 );
	std::array< int, p2.getX() > a2;
	ASSERT( a2.size() == 16 );
}

//------------------------------------------------------------------------------

namespace TemplateInstantiationDiscard
{
	template< int V >
	struct X
	{
		static_assert( V );
	};

	template< typename T >
	void action()
	{
		// without constexpr: error: static assertion failed
		if constexpr ( std::is_integral_v< T > )
			X< 1 >{};
		else
			X< 0 >{};
	}
}

DECLARE_TEST_CASE( ConstexprTest_IfConstexpr_TemplateInstantiationDiscard )
{
	TemplateInstantiationDiscard::action< int >();
	//TemplateInstantiationDiscard::action< double >(); // static assertion failed
}

//------------------------------------------------------------------------------

namespace TemplateInstantiationPointer
{
	template< typename T >
	auto getValue( T _t )
	{
		// does not compile without constexpr
		if constexpr ( std::is_pointer_v< T > )
			return * _t;
		else
			return _t;
	}
}

DECLARE_TEST_CASE( ConstexprTest_IfConstexpr_TemplateInstantiationPointer )
{
	int v{ 10 };
	int * pV{ & v };

	int r1{ TemplateInstantiationPointer::getValue( v ) };
	int r2{ TemplateInstantiationPointer::getValue( pV ) };
	ASSERT( r1 == 10 );
	ASSERT( r2 == 10 );

}

//------------------------------------------------------------------------------

namespace TemplateInstantiationMethodCall
{
	struct X
	{
		void onDelete()
		{
		}
	};

	template< typename T >
	void action( T * _ptr )
	{
		// without constexpr: request for member 'onDelete' in '* _ptr', which is of non-class type 'int'
		if constexpr ( std::is_class_v< T > )
			_ptr->onDelete();
	}
}

DECLARE_TEST_CASE( ConstexprTest_IfConstexpr_TemplateInstantiationMethodCall )
{
	int * pInt{};
	TemplateInstantiationMethodCall::X * pX{};
	TemplateInstantiationMethodCall::action( pInt );
	TemplateInstantiationMethodCall::action( pX );
}

//------------------------------------------------------------------------------

namespace HasApiMethod
{
	template< typename T >
	class HasCallableApi
	{
		template< typename R >
		static constexpr auto hasApiMethod( R * ) -> decltype( std::declval< R & >().api(), std::true_type{} );

		template< typename R >
		static constexpr auto hasApiMethod( ... ) -> decltype( std::false_type{} );

	public:

		static constexpr bool value{
			std::is_same_v<
					decltype( hasApiMethod< T >( nullptr ) )
				,	decltype( std::true_type{} )
			>
		};
	};

	class CallApi
	{
		template< bool _call >
		struct C
		{
			template< typename T >
			static constexpr auto call( T && )
			{
			}
		};
		template<>
		struct C< true >
		{
			template< typename T >
			static constexpr auto call( T && _t )
			{
				return _t.api();
			}
		};

	public:
		template< typename T >
		static constexpr auto call( T && _t )
		{
			return C< HasCallableApi< T >::value >::call( _t );
		}
	};

	template< typename T >
	constexpr auto call17( T && _t )
	{
		if constexpr ( HasCallableApi< T >::value )
			return _t.api();
	}
}

DECLARE_TEST_CASE( ConstexprTest_IfConstexpr_HasApiMethod )
{
	struct Func
	{
		double operator () ()
		{
			return 4.4;
		}
	};
	struct X1
	{
		int api;
	};
	struct X2
	{
		short api()
		{
			return 5;
		}
	};
	struct X3
	{
	};
	struct X4
	{
		Func api;
	};
	

	ASSERT( HasApiMethod::HasCallableApi< X1 >::value == false );
	ASSERT( HasApiMethod::HasCallableApi< X2 >::value == true );
	ASSERT( HasApiMethod::HasCallableApi< X3 >::value == false );
	ASSERT( HasApiMethod::HasCallableApi< X4 >::value == true );

	HasApiMethod::CallApi::call( X1{} );
	ASSERT( HasApiMethod::CallApi::call( X2{} ) == 5 );
	HasApiMethod::CallApi::call( X3{} );
	ASSERT( HasApiMethod::CallApi::call( X4{} ) == 4.4 );

	HasApiMethod::call17( X1{} );
	ASSERT( HasApiMethod::call17( X2{} ) == 5 );
	HasApiMethod::call17( X3{} );
	ASSERT( HasApiMethod::call17( X4{} ) == 4.4 );
}

//------------------------------------------------------------------------------

} // namespace Constexpr::Test
