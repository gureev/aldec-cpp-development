#ifndef __CONSTEXPR_STRLEN_HPP__
#define __CONSTEXPR_STRLEN_HPP__

//------------------------------------------------------------------------------

namespace Constexpr {

//------------------------------------------------------------------------------

constexpr int strlen( const char * _str )
{
	assert( _str );
	int result = 0;
	while ( *( _str++ ) )
		++result;
	return result;
}

//------------------------------------------------------------------------------

} // namespace Constexpr

//------------------------------------------------------------------------------

#endif // __CONSTEXPR_STRLEN_HPP__
