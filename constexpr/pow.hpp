#ifndef __CONSTEXPR_POW_HPP__
#define __CONSTEXPR_POW_HPP__

//------------------------------------------------------------------------------

namespace Constexpr {

//------------------------------------------------------------------------------

constexpr int pow( int _N, int _M )
{
	int result = 1;
	for ( int i = 0; i < _M; ++i )
		result *= _N;

	return result;
}

//------------------------------------------------------------------------------

} // namespace Constexpr

//------------------------------------------------------------------------------

#endif // __CONSTEXPR_POW_HPP__
