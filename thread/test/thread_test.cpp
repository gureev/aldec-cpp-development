#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

namespace Thread::Test {

//------------------------------------------------------------------------------

int doSomeWork( const std::string & )
{
	int r{};
	for ( int i{}; i < 100'000'000; ++i )
		r += 1;
	return r;
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ThreadTest_Thread )
{
	// NOTE: Sad, no result can be obtained
	std::thread t( doSomeWork, "someArg" );
	t.join();
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ThreadTest_Thread_Lambda )
{
	// NOTE: Result can be obtained through capture list

	int result{};

	std::thread t(
			[&]( const char * )
			{
				result = 10;
			}
		,	"someArg"
	);
	t.join();

	ASSERT( result == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ThreadTest_Thread_UseShortTimeout )
{
	std::thread t(
		[&]()
		{
			std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
		}
	);
	t.join();
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ThreadTest_AsyncTask_DefaultPolicy )
{
	// doSomeWork is called 'Task' is this context
	auto futureObject = std::async( doSomeWork, "someArg" );

	int result = futureObject.get();
	ASSERT( result == 100'000'000 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ThreadTest_AsyncTask_AsyncPolicy )
{
	// ensures 'doSomeWork' is executed in other thread
	auto futureObject = std::async( std::launch::async, doSomeWork, "someArg" );

	int result = futureObject.get();
	ASSERT( result == 100'000'000 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ThreadTest_AsyncTask_DefferedPolicy )
{
	// 'doSomeWork' is executed in thread that calls 'get'
	auto futureObject = std::async( std::launch::deferred, doSomeWork, "someArg" );

	int result = futureObject.get();
	ASSERT( result == 100'000'000 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ThreadTest_AsyncTask_UseShortTimeout )
{
	auto futureObject = std::async( std::launch::async, doSomeWork, "someArg" );

	while ( futureObject.wait_for( std::chrono::milliseconds( 100 ) ) != std::future_status::ready );
	
	int result = futureObject.get();
	ASSERT( result == 100'000'000 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ThreadTest_AsyncTask_UseShortTimeout_DefaultPolicy )
{
	auto futureObject = std::async( doSomeWork, "someArg" );

	if ( futureObject.wait_for( std::chrono::seconds( 0 ) ) == std::future_status::deferred )
	{
		// something is wrong to call task in async thread
		std::cout << "Deferred" << std::endl;
	}
	else
	{
		// okay, task is asynchronously executed. wait for result
		std::cout << "Asynchronous" << std::endl;
		while ( futureObject.wait_for( std::chrono::milliseconds( 100 ) ) != std::future_status::ready );
	}
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ThreadTest_ThreadLocal_AsyncTask_LambdaMember )
{
	auto fun = []()
	{
		thread_local int tls_value{};
		++tls_value;
		return tls_value;
	};
	auto v1 = std::async( std::launch::async, fun ).get();
	auto v2 = std::async( std::launch::async, fun ).get();
	ASSERT( v1 == 1 );
	ASSERT( v2 == 2 ); // VS sucks
}

//------------------------------------------------------------------------------

struct ThreadLocalStructureAsyncTask
{
	static thread_local int m_x;
};
thread_local int ThreadLocalStructureAsyncTask::m_x{};

DECLARE_TEST_CASE( ThreadTest_ThreadLocal_AsyncTask_StaticClassMember )
{
	auto fun = []()
	{
		++ThreadLocalStructureAsyncTask::m_x;
		return ThreadLocalStructureAsyncTask::m_x;
	};
	ThreadLocalStructureAsyncTask::m_x = 55;

	auto v1 = std::async( std::launch::async, fun ).get();
	auto v2 = std::async( std::launch::async, fun ).get();
	ASSERT( v1 == 1 );
	ASSERT( v2 == 2 ); // VS sucks
}

//------------------------------------------------------------------------------

thread_local int g_x{};

DECLARE_TEST_CASE( ThreadTest_ThreadLocal_AsyncTask_GlobalMember )
{
	auto fun = []()
	{
		++g_x;
		return g_x;
	};
	g_x = 55;

	auto v1 = std::async( std::launch::async, fun ).get();
	auto v2 = std::async( std::launch::async, fun ).get();
	ASSERT( v1 == 1 );
	ASSERT( v2 == 2 ); // VS sucks
}

//------------------------------------------------------------------------------

int f_x()
{
	thread_local int gl_x{};
	++gl_x;
	return gl_x;
}

DECLARE_TEST_CASE( ThreadTest_ThreadLocal_AsyncTask_FunctionMember )
{
	auto v1 = std::async( std::launch::async, f_x ).get();
	auto v2 = std::async( std::launch::async, f_x ).get();
	ASSERT( v1 == 1 );
	ASSERT( v2 == 2 ); // VS sucks
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ThreadTest_ThreadLocal_Thread_LambdaMember )
{
	int r{};
	auto fun = [ &r ]()
	{
		thread_local int tls_value{};
		++tls_value;
		r = tls_value;
	};
	std::thread( fun ).join();
	ASSERT( r == 1 );
	r = 0;
	std::thread( fun ).join();
	ASSERT( r == 1 );
}

//------------------------------------------------------------------------------

struct ThreadLocalStructureThread
{
	static thread_local int m_x;
};
thread_local int ThreadLocalStructureThread::m_x{};

DECLARE_TEST_CASE( ThreadTest_ThreadLocal_Thread_StaticClassMember )
{
	int r{};
	auto fun = [ &r ]()
	{
		++ThreadLocalStructureThread::m_x;
		r = ThreadLocalStructureThread::m_x;
	};
	ThreadLocalStructureThread::m_x = 55;

	std::thread( fun ).join();
	ASSERT( r == 1 );
	r = 0;
	std::thread( fun ).join();
	ASSERT( r == 1 );
}

//------------------------------------------------------------------------------

thread_local int g_y{};

DECLARE_TEST_CASE( ThreadTest_ThreadLocal_Thread_GlobalMember )
{
	int r{};
	auto fun = [ &r ]()
	{
		++g_y;
		r = g_y;
	};
	g_y = 55;

	std::thread( fun ).join();
	ASSERT( r == 1 );
	r = 0;
	std::thread( fun ).join();
	ASSERT( r == 1 );
}

//------------------------------------------------------------------------------

int f_y()
{
	thread_local int gl_y{};
	++gl_y;
	return gl_y;
}

DECLARE_TEST_CASE( ThreadTest_ThreadLocal_Thread_FunctionMember )
{
	int r{};
	auto fun = [ &r ]()
	{
		r = f_y();
	};
	std::thread( fun ).join();
	ASSERT( r == 1 );
	r = 0;
	std::thread( fun ).join();
	ASSERT( r == 1 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ThreadTest_UnjoinableThread )
{
	//auto th = std::thread( f_y );
	// Either .join(), or .detach() must be called for 'th'
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ThreadTest_AutoThread )
{
	struct AutoThread final
	{
		enum class Policy
		{
				Join
			,	Detach
		};

		AutoThread( std::thread && _thread, Policy _policy )
			:	m_thread( std::move( _thread ) )
			,	m_policy( _policy )
		{
		}

		~AutoThread()
		{
			if ( !m_thread.joinable() )
				return;

			if ( m_policy == Policy::Join )
				m_thread.join();
			else
				m_thread.detach();
		}

		std::thread & get()
		{
			return m_thread;
		}

	private:

		std::thread m_thread;
		Policy m_policy;
	};

	auto fun = []()
	{
	};

	AutoThread thread1( std::thread( fun ), AutoThread::Policy::Join );
	AutoThread thread2( std::thread( fun ), AutoThread::Policy::Detach );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( ThreadTest_AsyncWithoutFuture )
{
	auto fun = []()
	{
	};

	auto future1 = std::async( std::launch::async, fun );
	auto future2 = std::async( std::launch::deferred, fun );
	auto future3 = std::async( fun );
}

//------------------------------------------------------------------------------

} // namespace Thread::Test
