#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

void __stdcall stdf()
{
}

DECLARE_TEST_CASE( CallingConvention )
{
	using fptr_t = void ( __cdecl * )();
	//fptr_t fptr = stdf; // does not work
}

//------------------------------------------------------------------------------
