#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

namespace Overview::Cpp14 {

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp14_IntegerSequence )
{
	// type traits
	std::integer_sequence< int, 1, 2, 3 > is;
	ASSERT( is.size() == 3 );

	// type alias
	ASSERT(
		std::is_same<
				std::integer_sequence< std::size_t, 1, 2, 3 >
			,	std::index_sequence< 1, 2, 3 >
		>::value
	);

	auto is2 = std::make_integer_sequence< int, 2 >();
	ASSERT( is2.size() == 2 );
}

//------------------------------------------------------------------------------

template<
		typename T
	,	typename R = std::conditional_t<
				std::is_same< T, int >::value
			,	std::integral_constant< int, 10 >
			,	std::integral_constant< int, 5 >
		>
>
T variableTemplate = R::value;

DECLARE_TEST_CASE( OverviewCpp14_VariableTemplate )
{
	// NOTE: Applies only to variables (local or global) and static data members

	ASSERT( variableTemplate< short > == 5 );
	ASSERT( variableTemplate< int > == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp14_DecltypeAuto )
{
	int a = 0;
	decltype( auto ) b = a; // int

	ASSERT( std::is_same< decltype( b ), int >::value == true );
	ASSERT( std::is_same< decltype( b ), const int & >::value == false );

	const int & ra = a;
	decltype( auto ) rb = ra; // const int &

	ASSERT( std::is_same< decltype( rb ), int >::value == false );
	ASSERT( std::is_same< decltype( rb ), const int & >::value == true );
}

//------------------------------------------------------------------------------

template< typename T1, typename T2 >
auto ReturnTypeDeduction( T1 _t1, T2 _t2 )
{
	return _t1 + _t2;
}

DECLARE_TEST_CASE( OverviewCpp14_ReturnTypeDeduction )
{
	decltype( auto ) r1 = ReturnTypeDeduction( 10, 20 );
	decltype( auto ) r2 = ReturnTypeDeduction( 15.5, 20 );

	ASSERT( r1 == 30 );
	ASSERT( r2 == 35.5 );
	ASSERT( std::is_same< decltype( r1 ), int >::value == true );
	ASSERT( std::is_same< decltype( r2 ), double >::value == true );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp14_LambdaCapture )
{
	int v = 10;
	auto lambda = [x = v]()
	{
		return x;
	};
	v = 15;

	ASSERT( lambda() == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp14_GenericLambda )
{
	auto lambda = []( auto _p1 )
	{
		return _p1;
	};

	ASSERT( lambda( 10 ) == 10 );
	ASSERT( lambda( 11.1 ) == 11.1 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp14_GenericVariadicLambda )
{
	auto lambda = []( auto _p1, auto ... _pN )
	{
		auto result = _p1;
		auto args = { _pN ... };
		for ( auto v : args )
			result += v;
		return result;
	};

	ASSERT( lambda( 10, 0 ) == 10 );
	ASSERT( lambda( 10, 15 ) == 25 );
	ASSERT( lambda( 10, 1, 2 ) == 13 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp14_BinaryLiteral )
{
	int dec = 10;
	int octal = 012;
	int hex = 0xA;
	int binary = 0b1010;

	ASSERT( dec == 10 );
	ASSERT( octal == 10 );
	ASSERT( hex == 10 );
	ASSERT( binary == 10 );

	int longValue = 10'000;
	ASSERT( longValue == 10000 );
	ASSERT( longValue == 10'000 );
	ASSERT( longValue == 1'0'0'0'0 );
}

//------------------------------------------------------------------------------

} // namespace Overview::Cpp14
