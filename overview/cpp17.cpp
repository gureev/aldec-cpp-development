#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

namespace Overview::Cpp17 {

//------------------------------------------------------------------------------

struct X
{
	X()
	{
		++m_default;
	}
	X( const X & _x )
	{
		m_default = _x.m_default;
		m_copy = _x.m_copy;
		m_move = _x.m_move;

		++m_copy;
	}
	X( X && _x )
	{
		m_default = _x.m_default;
		m_copy = _x.m_copy;
		m_move = _x.m_move;

		++m_move;
	}
	int m_default{ 0 };
	int m_copy{ 0 };
	int m_move{ 0 };
};
X f()
{
	return X{};
};

DECLARE_TEST_CASE( OverviewCpp17_CopyElision )
{
	/*
		NRVO - optimization
		RVO - optimization (until C++17) and language specification (since C++17)
	*/

	X x = X( X( X( X() ) ) );
	ASSERT( x.m_default == 1 );
	ASSERT( x.m_copy == 0 );
	ASSERT( x.m_move == 2 ); // Should be 0. Compiler is required to omit copy/move

	X y = f();
	ASSERT( y.m_default == 1 );
	ASSERT( y.m_copy == 0 );
	ASSERT( y.m_copy == 0 );

	X * pY = new X( f() );
	ASSERT( pY->m_default == 1 );
	ASSERT( pY->m_copy == 0 );
	ASSERT( pY->m_copy == 0 );
	delete pY;
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_MapSetSplice )
{
	std::map< int, int > m1{ { 1, 1 }, { 2, 2 }, { 3, 3 } };
	std::map< int, int > m2{ { 4, 4 }, { 5, 5 } };
	
	std::map< int, int > m1c = m1;
	std::map< int, int > m2c = m2;
	m1c.merge( m2c );
	ASSERT( m1c.size() == 5 );
	ASSERT( m2c.size() == 0 );

	auto nodeHandler = m2.extract( 4 );
	ASSERT( nodeHandler.empty() == false );
	ASSERT( nodeHandler.key() == 4 );
	ASSERT( nodeHandler.mapped() == 4 );

	nodeHandler.key() = 44;
	m1.insert( std::move( nodeHandler ) );
	ASSERT( m1.find( 44 ) != m1.end() );

	auto nodeHandler2 = m2.extract( 10 );
	ASSERT( nodeHandler2.empty() == true );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_Apply )
{
	int x, y, z;
	auto handler = [&]( int _x, int _y, int _z )
	{
		x = _x;
		y = _y;
		z = _z;
	};
	std::apply( handler, std::make_tuple( 10, 20, 30 ) );

	ASSERT( x == 10 );
	ASSERT( y == 20 );
	ASSERT( z == 30 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_StdByte )
{
	[[ maybe_unused ]] std::byte byte{ 0 };

	ASSERT( sizeof( std::byte ) == sizeof( unsigned char ) );
	ASSERT( alignof( std::byte ) == alignof( unsigned char ) );
}

//------------------------------------------------------------------------------

constexpr auto testIfConstexpr()
{
	if constexpr ( std::is_integral< int >::value )
	{
		[[ maybe_unused ]] int dummy{ 0 };
		return true;
	}
	else
	{
		[[ maybe_unused ]] int dummy{ 0 };
		return false;
	}
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_IfConstexpr )
{
	static_assert( testIfConstexpr() == true ); // Actually works without constexpr
}

//------------------------------------------------------------------------------

template< int /*auto*/ _Value > // Has not been supported yet
struct ClassAutoTemplate
{
	static constexpr auto Value{ _Value };
};

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_ClassAutoTemplate )
{
	ClassAutoTemplate< 10 > cat;
	ASSERT( cat.Value == 10 );
}

//------------------------------------------------------------------------------

template< typename T = int >
struct ClassTemplateArgumentDeduction
{
	ClassTemplateArgumentDeduction( const T & _t )
	{
	}
};

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_ClassTemplateArgumentDeduction )
{
	//ClassTemplateArgumentDeduction ctad{ 10 }; // Has not been supported yet
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_FoldExpression )
{
	// MetaTest_FoldExpressions_UnaryFold
	// MetaTest_FoldExpressions_BinaryFold
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_InlineVariable )
{
	int someValue{ 10 };
	[[ maybe_unused ]] /*inline*/ int someInlineValue{ someValue }; // Has not been supported yet
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_StructureBinding )
{
	std::tuple< int, int > tuple{ 10, 20 };
	const auto & [ x1, y1 ] = tuple;
	ASSERT( x1 == 10 );
	ASSERT( y1 == 20 );

	std::pair< int, int > pair{ 30, 40 };
	const auto & [ x2, y2 ] = pair;
	ASSERT( x2 == 30 );
	ASSERT( y2 == 40 );

	std::array< int, 2 > array{ 50, 60 };
	const auto & [ x3, y3 ] = array;
	ASSERT( x3 == 50 );
	ASSERT( y3 == 60 );

	struct SomeTuple
	{
		int m_value1{ 70 };
		int m_value2{ 80 };
	};
	SomeTuple st{};
	const auto & [ x4, y4 ] = st;
	ASSERT( x4 == 70 );
	ASSERT( y4 == 80 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_IfInitializer )
{
	struct X
	{
		bool m_value;
	};
	struct Y
	{
		X getX()
		{
			return { true };
		}
	};

	Y y;
	if ( X r{ y.getX() }; !r.m_value )
	{
		ASSERT( false );
		r.m_value = r.m_value;
	}
	else
	{
		r.m_value = r.m_value;
	}
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_SwitchInitializer )
{
	struct X
	{
		bool m_value;
	};
	struct Y
	{
		X getX()
		{
			return { true };
		}
	};

	Y y;
	switch ( X r{ y.getX() }; r.m_value )
	{
		case true:
			r.m_value = r.m_value;
			break;

		default:
			r.m_value = r.m_value;
			ASSERT( false );
			break;
	}
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_EnumInitialization )
{
	enum class Enum : char
	{
	};
	[[ maybe_unused ]] Enum e1{ 10 };
	//[[ maybe_unused ]] Enum e2 = 10; // does not compile
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_Variant )
{
	std::variant< int, double > v{ 5 }; // without heap memory
	try
	{
		std::get< double >( v );
	}
	catch ( const std::bad_variant_access & )
	{
	}

	ASSERT( std::get< int >( v ) == 5 );
	ASSERT( std::get< 0 >( v ) == 5 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_StdOptional )
{
	std::optional< int > opt;
	ASSERT( !opt.has_value() );
	ASSERT( opt.value_or( 123 ) == 123 );

	opt = 5;
	ASSERT( opt.has_value() );
	ASSERT( opt.value_or( 123 ) == 5 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_Any )
{
	std::any a;
	ASSERT( !a.has_value() );

	a = 5;
	ASSERT( a.has_value() );
	ASSERT( std::any_cast< int >( a ) == 5 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_StringView )
{
	std::string str{ "str_str" };
	std::string_view strView{ str };
	strView.remove_suffix( 1 );
	strView.remove_prefix( 2 );

	ASSERT( str == "str_str" );
	ASSERT( strView == "r_st" );

	str[ 3 ] = '+';
	ASSERT( str == "str+str" );
	ASSERT( strView == "r+st" );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp17_BinaryLiteral )
{
	ASSERT( 0b111 == 7 );
}

//------------------------------------------------------------------------------

} // namespace Overview::Cpp17
