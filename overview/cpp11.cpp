#include "ph/ph.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

namespace Overview::Cpp11 {

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_SharedPtr )
{
	std::shared_ptr< int > p1( new int( 10 ) );
	std::shared_ptr< int > p2( std::make_shared< int >( 11 ) );
	[[ maybe_unused ]] std::shared_ptr< int > p3( std::make_unique< int >( 12 ) );

	ASSERT( *p1 == 10 );
	ASSERT( p1.use_count() == 1 );

	ASSERT( *p2 == 11 );
	ASSERT( p2.use_count() == 1 );

	std::weak_ptr< int > wp2;
	ASSERT( wp2.expired() == true );
	ASSERT( wp2.lock() == false );

	wp2 = p2;
	ASSERT( wp2.expired() == false );
	ASSERT( wp2.lock() == p2 );

	p2.reset();
	ASSERT( wp2.expired() == true );
	ASSERT( wp2.lock() == false );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_UnorderedConstainers )
{
	std::unordered_map< int, int > a1;
	std::unordered_set< int, int > a2;
	std::unordered_multimap< int, int > a3;
	std::unordered_multiset< int, int > a4;

	std::unordered_map<
			int
		,	int
		,	std::hash< int /*key type*/ >
		,	std::equal_to< int /*key type*/ >
	> a5;
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_Array )
{
	std::array< int, 3 > a = { 1, 2, 3 };
	ASSERT( a.size() == 3 );
	ASSERT( a[ 0 ] == 1 );
	ASSERT( a.back() == 3 );

	a.fill( 5 );
	ASSERT( a == std::array< int, 3 >{ 5, 5, 5 } );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_Tupple )
{
	std::pair< int, double > p = { 10, 21.1 };
	ASSERT( p.first == 10 );
	ASSERT( p.second = 21.1 );

	std::tuple< int, double, int > t = { 8, 1.1, 5 };
	ASSERT( std::get< 0 >( t ) == 8 );
	ASSERT( std::get< 1 >( t ) == 1.1 );
	ASSERT( std::get< 2 >( t ) == 5 );

	int t1 = -1;
	int t3 = -1;
	std::tie( t1, std::ignore, t3 ) = t;
	ASSERT( t1 == 8 );
	ASSERT( t3 == 5 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_Chrono )
{
	auto timePoint = std::chrono::system_clock::now();
	std::chrono::time_point< std::chrono::system_clock > timePoint2 = std::chrono::system_clock::now();

	std::chrono::duration< double > elapsed = timePoint2 - timePoint;
	ASSERT( elapsed.count() >= 0.0 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_TypeTraits )
{
	ASSERT( std::is_integral< int >::value == true );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_Move )
{
	std::string a = "hello";
	std::string b = std::move( a );
	ASSERT( a == "" );
	ASSERT( b == "hello" );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_Forward )
{
	std::string a = "hello";
	std::string b = std::forward< std::string & >( a );
	ASSERT( a == "hello" );
	ASSERT( b == "hello" );
	std::string c = std::forward< std::string && >( a );
	ASSERT( a == "" );
	ASSERT( b == "hello" );
	ASSERT( c == "hello" );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_ClassFieldInitializer )
{
	struct MyClass
	{
		int v1 = 11;
		int v2{ 12 };
	};
	ASSERT( MyClass().v1 == 11 );
	ASSERT( MyClass().v2 == 12 );
}

//------------------------------------------------------------------------------

namespace TestNamespace
{
	namespace SomeNested
	{
		int test() { return 0; }
	}
	inline namespace InlineNested
	{
		int test() { return 1; }
	}
}

DECLARE_TEST_CASE( OverviewCpp11_InlineNamespace )
{
	ASSERT( TestNamespace::SomeNested::test() == 0 );
	ASSERT( TestNamespace::InlineNested::test() == 1 );
	ASSERT( TestNamespace::test() == 1 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_ExplcitTypeConversionOperator )
{
	struct MyClass
	{
		explicit operator bool () const { return false; }
	};

	if ( MyClass() )
	{
		ASSERT( false );
	}
	while ( MyClass() )
	{
		ASSERT( false );
	}
	// bool r = MyClass(); // impossible because of 'explicit'
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_InitializerList )
{
	std::initializer_list< int > l = { 1, 2, 3 };
	ASSERT( l.size() == 3 );
	ASSERT( *l.begin() == 1 );
	ASSERT( *(l.begin()+1) == 2 );
	ASSERT( *(l.begin()+2) == 3 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_ConvertingConstructors )
{
	struct MyClass1
	{
		MyClass1( int _a, int _b )
		{
			m_a = _a;
			m_b = _b;
		}
		int m_a;
		int m_b;
	};

	MyClass1 mc1 = { 1, 2 };
	ASSERT( mc1.m_a == 1 );
	ASSERT( mc1.m_b == 2 );

	struct MyClass2
	{
		MyClass2( int, int )
		{
			ASSERT( false );
		}
		MyClass2( std::initializer_list< int > _list )
		{
			m_a = *_list.begin();
			m_b = *( _list.begin() + 1 );
		}
		int m_a;
		int m_b;
	};

	MyClass2 mc2 = { 1, 2 };
	ASSERT( mc2.m_a == 1 );
	ASSERT( mc2.m_b == 2 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_RangeBasedLoop )
{
	for ( int v : { 1 } )
		ASSERT( v == 1 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_DeletedMethod )
{
	struct MyClass
	{
		MyClass() = delete;
	};

	//MyClass mc1; // impossible
}

//------------------------------------------------------------------------------

void fd( int )
{
}
void fd( char ) = delete;

DECLARE_TEST_CASE( OverviewCpp11_DeletedFunction )
{
	fd( 10 );
	//fd( 'a' ); // fd( int ) without delete, impossible with delete
}

//------------------------------------------------------------------------------

template< typename T >
void tfd( T )
{
}
template<>
void tfd< double >( double ) = delete;

DECLARE_TEST_CASE( OverviewCpp11_DeletedTemplateFunction )
{
	tfd( 10 );
	//tfd( 5.5 ); // tfd( T ) without delete, impossible with delete
}

//------------------------------------------------------------------------------

template< typename T >
void tfdr( T ) = delete;
template<>
void tfdr< double >( double )
{
}

DECLARE_TEST_CASE( OverviewCpp11_DeletedTemplateFunction_Reversed )
{
	//tfdr( 10 ); // impossible
	tfdr( 5.5 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_DefaultMethod )
{
	struct MyClass
	{
		MyClass() = default;
	};

	[[ maybe_unused ]] MyClass mc1;
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_OverrideMethod )
{
	struct MyClass
	{
		virtual void f()
		{
		}
	};
	struct Derived : public MyClass
	{
		void f() override
		{
		}
	};
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_FinalMethod )
{
	struct MyClass
	{
		virtual void f()
		{
		}
	};
	struct Derived : public MyClass
	{
		void f() final
		{
		}
	};
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_FinalClass )
{
	struct MyClass
	{
	};
	struct Derived final : public MyClass
	{
	};
}

//------------------------------------------------------------------------------

/*
ReturnType operator "" _a(unsigned long long int);   // Literal operator for user-defined INTEGRAL literal
ReturnType operator "" _b(long double);              // Literal operator for user-defined FLOATING literal
ReturnType operator "" _c(char);                     // Literal operator for user-defined CHARACTER literal
ReturnType operator "" _d(wchar_t);                  // Literal operator for user-defined CHARACTER literal
ReturnType operator "" _e(char16_t);                 // Literal operator for user-defined CHARACTER literal
ReturnType operator "" _f(char32_t);                 // Literal operator for user-defined CHARACTER literal
ReturnType operator "" _g(const     char*, size_t);  // Literal operator for user-defined STRING literal
ReturnType operator "" _h(const  wchar_t*, size_t);  // Literal operator for user-defined STRING literal
ReturnType operator "" _i(const char16_t*, size_t);  // Literal operator for user-defined STRING literal
ReturnType operator "" _g(const char32_t*, size_t);  // Literal operator for user-defined STRING literal
ReturnType operator "" _r(const char*);              // Raw literal operator
template<char...> ReturnType operator "" _t();       // Literal operator template
*/
unsigned long long int operator "" _p1( unsigned long long int _value )
{
	return _value + 1;
}

DECLARE_TEST_CASE( OverviewCpp11_UserDefinedLiteral )
{
	ASSERT( 10_p1 == 11 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_DelegatingConstructors )
{
	struct MyClass
	{
		MyClass()
			: m_v( 10 )
		{
		}
		MyClass( int )
			: MyClass()
		{
		}
		int m_v;
	};
	ASSERT( MyClass().m_v == 10 );
	ASSERT( MyClass( 20 ).m_v == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_Constexpr )
{
	struct MyClass
	{
		int m_v;
		constexpr MyClass( int _v )
			: m_v( _v )
		{
		}
	};

	char data[ MyClass( 10 ).m_v ];
	ASSERT( sizeof( data ) == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_EnumClass )
{
	enum class E1
	{
		V
	};
	enum class E2
	{
		V
	};
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_Nullptr )
{
	[[ maybe_unused ]] void * p1 = 0;
	[[ maybe_unused ]] void * p2 = NULL;
	[[ maybe_unused ]] void * p3 = nullptr;
}

//------------------------------------------------------------------------------

template< typename T >
using MyVector = std::vector< T >;

DECLARE_TEST_CASE( OverviewCpp11_TypeAlias )
{
	typedef int( *pF1 )( bool, short );
	typedef int MyInt1;

	using pF2 = int( * )( bool, short );
	using MyInt2 = int;

	ASSERT( std::is_same< pF1, pF2 >::value == true );
	ASSERT( std::is_same< MyInt1, MyInt2 >::value == true );
	ASSERT( std::is_same< MyInt1, int >::value == true );
	ASSERT( std::is_same< MyVector< int >, std::vector< int > >::value == true );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_Auto )
{
	int a = 0;
	auto b = a; // int

	ASSERT( std::is_same< decltype( b ), int >::value == true );
	ASSERT( std::is_same< decltype( b ), const int & >::value == false );

	const int & ra = a;
	auto rb = ra; // int

	ASSERT( std::is_same< decltype( rb ), int >::value == true );
	ASSERT( std::is_same< decltype( rb ), const int & >::value == false );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_Decltype )
{
	int a = 0;
	decltype( a ) b = a; // int

	ASSERT( std::is_same< decltype( b ), int >::value == true );
	ASSERT( std::is_same< decltype( b ), const int & >::value == false );

	const int & ra = a;
	decltype( ra ) rb = ra; // const int &

	ASSERT( std::is_same< decltype( rb ), int >::value == false );
	ASSERT( std::is_same< decltype( rb ), const int & >::value == true );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_Lambda )
{
	auto lambda = []()
	{
		return 12;
	};

	ASSERT( lambda() == 12 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_StaticAssert )
{
	int a = 10;
	static_assert( std::is_same< decltype( a ), int >::value == true, "Message in case of failure" );
}

//------------------------------------------------------------------------------

template< typename ... T >
struct MyVariadicTemplate
{
	constexpr static int size = sizeof...( T );
};

DECLARE_TEST_CASE( OverviewCpp11_VariadicTemplate )
{
	ASSERT( MyVariadicTemplate<>::size == 0 );
	ASSERT( MyVariadicTemplate< int, double >::size == 2 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( OverviewCpp11_AlignOf )
{
	struct AlignMixData
	{
		int m_a;
		double m_b;
		char m_c;
	};
	struct AlignCharData
	{
		char m_a;
		char m_b;
		char m_c;
	};
	struct AlignIntegerData
	{
		int m_a;
		char m_b;
	};
	struct AlignEmpty
	{
	};
	struct alignas( 32 ) AlignLargeEmpty
	{
	};

	ASSERT( sizeof( int ) == 4 );
	ASSERT( sizeof( double ) == 8 );
	ASSERT( sizeof( char ) == 1 );
#ifdef _WIN64
	ASSERT( sizeof( void * ) == 8 );
#else
	ASSERT( sizeof( void * ) == 4 );
#endif
#ifdef _WIN64
	ASSERT( sizeof( AlignMixData ) == 24 );
#else
	ASSERT( sizeof( AlignMixData ) == 16 );
#endif
	ASSERT( sizeof( AlignCharData ) == 3 );
	ASSERT( sizeof( AlignIntegerData ) == 8 );
	ASSERT( sizeof( AlignEmpty ) == 1 );
	ASSERT( sizeof( AlignLargeEmpty ) == 32 );

	ASSERT( alignof( int ) == 4 );
	ASSERT( alignof( double ) == 8 );
	ASSERT( alignof( char ) == 1 );
#ifdef _WIN64
	ASSERT( alignof( void * ) == 8 );
#else
	ASSERT( alignof( void * ) == 4 );
#endif
#ifdef _WIN64
	ASSERT( alignof( AlignMixData ) == 8 );
#else
	ASSERT( alignof( AlignMixData ) == 4 );
#endif
	ASSERT( alignof( AlignCharData ) == 1 );
	ASSERT( alignof( AlignIntegerData ) == 4 );
	ASSERT( alignof( AlignEmpty ) == 1 );
	ASSERT( alignof( AlignLargeEmpty ) == 32 );

	// Can be: 2 x sizeof( void * )
	// Can be: alignof( max_align_t ) where max_align_t is double
	// Can be: implementation-defined
#ifdef _WIN64
	ASSERT( __STDCPP_DEFAULT_NEW_ALIGNMENT__ == 16 );
#else
	ASSERT( __STDCPP_DEFAULT_NEW_ALIGNMENT__ == 8 );
#endif

	//-----

	union UnalignmentStorage
	{
		//int alignment;
		char m_data[ sizeof( int ) * 2 ];
	};
	ASSERT( sizeof( UnalignmentStorage ) == 8 );
	ASSERT( alignof( UnalignmentStorage ) == 1 );

	union AlignmentStorage
	{
		int alignment;
		char m_data[ sizeof( int ) * 2 ];
	};
	ASSERT( sizeof( AlignmentStorage ) == 8 );
	ASSERT( alignof( AlignmentStorage ) == 4 );

	//-----

	{
		[[ maybe_unused ]] char a;
		char b[ sizeof( int ) ];

		ASSERT( sizeof( b ) == 4 );
		ASSERT( alignof( decltype( b ) ) == 1 );
		ASSERT( reinterpret_cast< int >( b ) % ( alignof( int ) ) == 0 );
	}

	//-----

	{
		struct SSS
		{
			char a;
			char b[ sizeof( int ) ];
		};
		SSS sss;

		ASSERT( sizeof( sss ) == 5 );
		ASSERT( alignof( SSS ) == 1 );
		//ASSERT( alignof( SSS::a ) == 1 ); // dont compile, only hint is usable
		//ASSERT( alignof( SSS::b ) == 1 ); // dont compile, only hint is usable
		//ASSERT( reinterpret_cast< int >( sss.b ) % ( alignof( int ) ) == 0 ); // FAILS
	}

	//-----

	{
		struct alignas( int ) SSS
		{
			char a;
			char b[ sizeof( int ) ];
		};
		SSS sss;

		ASSERT( sizeof( sss ) == 8 );
		ASSERT( alignof( SSS ) == 4 );
		//ASSERT( alignof( SSS::a ) == 1 ); // dont compile, only hint is usable
		//ASSERT( alignof( SSS::b ) == 1 ); // dont compile, only hint is usable
		//ASSERT( reinterpret_cast< int >( sss.b ) % ( alignof( int ) ) == 0 ); // FAILS
	}

	//-----

	{
		struct SSS
		{
			char a;
			alignas( int ) char b[ sizeof( int ) ];
		};
		SSS sss;

		ASSERT( sizeof( sss ) == 8 );
		ASSERT( alignof( SSS ) == 4 );
		//ASSERT( alignof( SSS::a ) == 1 ); // dont compile, only hint is usable
		//ASSERT( alignof( SSS::b ) == 4 ); // dont compile, only hint is usable
		ASSERT( reinterpret_cast< int >( sss.b ) % ( alignof( int ) ) == 0 );
	}

	//-----

	{
		struct SSS
		{
			char a;
			char * b = new char[ sizeof( int ) ];
		};
		SSS sss;

#ifdef _WIN64
		ASSERT( sizeof( sss ) == 16 );
#else
		ASSERT( sizeof( sss ) == 8 );
#endif
#ifdef _WIN64
		ASSERT( alignof( SSS ) == 8 );
#else
		ASSERT( alignof( SSS ) == 4 );
#endif
		//ASSERT( alignof( SSS::a ) == 1 ); // dont compile, only hint is usable
		//ASSERT( alignof( SSS::b ) == 4 ); // dont compile, only hint is usable
		ASSERT( reinterpret_cast< int >( sss.b ) % ( alignof( int ) ) == 0 );
	}

	//-----

	// Case with struct sss can be revised also as global structure
}

//------------------------------------------------------------------------------

} // namespace Overview::Cpp11
