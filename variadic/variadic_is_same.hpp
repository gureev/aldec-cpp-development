#ifndef __VARIADIC_IS_SAME_HPP__
#define __VARIADIC_IS_SAME_HPP__

//------------------------------------------------------------------------------

#include "meta/is_same.hpp"

//------------------------------------------------------------------------------

namespace Variadic {

//------------------------------------------------------------------------------

template< typename T, typename ... Next >
struct VariadicIsSame
{
	using Type = T;
	static const bool value =
			VariadicIsSame< Next... >::value
		&	Meta::IsSame< T, typename VariadicIsSame< Next... >::Type >::value;
};

//------------------------------------------------------------------------------

template< typename T >
struct VariadicIsSame< T >
{
	using Type = T;
	static const bool value = true;
};

//------------------------------------------------------------------------------

} // namespace Variadic

//------------------------------------------------------------------------------

#endif // __VARIADIC_IS_SAME_HPP__
