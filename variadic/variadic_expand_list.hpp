#ifndef __VARIADIC_EXPAND_LIST_HPP__
#define __VARIADIC_EXPAND_LIST_HPP__

//------------------------------------------------------------------------------

#include "meta/is_same.hpp"

#include "variadic/variadic_is_same.hpp"

//------------------------------------------------------------------------------

namespace Variadic {

//------------------------------------------------------------------------------

template< typename T >
struct ExpandList
{
	std::vector< T > m_list;

	template< typename ... R >
	void expand( R ... _values )
	{
		static_assert( Variadic::VariadicIsSame< R... >::value, "0xBAD" );
		m_list = { _values ... };
	}
};

//------------------------------------------------------------------------------

} // namespace Variadic

//------------------------------------------------------------------------------

#endif // __VARIADIC_EXPAND_LIST_HPP__
