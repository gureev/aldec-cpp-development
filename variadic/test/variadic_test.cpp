#include "ph/ph.hpp"

#include "variadic/variadic_size.hpp"
#include "variadic/variadic_is_same.hpp"
#include "variadic/variadic_expand_list.hpp"
#include "variadic/visitor.hpp"

#include "test/test_case.hpp"

//------------------------------------------------------------------------------

namespace Variadic::Test {

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( VariadicTest_Size )
{
	ASSERT( Variadic::VariadicSize<>::value == 0 );
	ASSERT( Variadic::VariadicSize< int >::value == 1 );
	ASSERT( Variadic::VariadicSize< int, int, double >::value == 3 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( VariadicTest_IsSame )
{
	ASSERT( Variadic::VariadicIsSame< int >::value == true );
	ASSERT( Variadic::VariadicIsSame< int, int, int >::value == true );

	ASSERT( Variadic::VariadicIsSame< double, int, int >::value == false );
	ASSERT( Variadic::VariadicIsSame< int, double, int >::value == false );
	ASSERT( Variadic::VariadicIsSame< int, int, double >::value == false );

	ASSERT( Variadic::VariadicIsSame< double, double, int >::value == false );
	ASSERT( Variadic::VariadicIsSame< int, double, double >::value == false );
	ASSERT( Variadic::VariadicIsSame< double, int, double >::value == false );

	ASSERT( Variadic::VariadicIsSame< double, int, double >::value == false );
	ASSERT( Variadic::VariadicIsSame< double, double, int >::value == false );
	ASSERT( Variadic::VariadicIsSame< double, int, double >::value == false );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( VariadicTest_ExpandList )
{
	Variadic::ExpandList< int > list;
	list.expand( 10 );
	ASSERT( list.m_list.size() == 1 );
	ASSERT( *( list.m_list.begin() ) == 10 );

	list.expand( 10, 20 );
	ASSERT( list.m_list.size() == 2 );
	ASSERT( *( list.m_list.begin() ) == 10 );
	ASSERT( *( ++list.m_list.begin() ) == 20 );

	//list.expand( 10, static_cast< short >( 5 ) ); // error C2338: 0xBAD
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( VariadicTest_OptimizationFatal )
{
	std::initializer_list< int > list{ 5, 10 };
	/*std::initializer_list< int > list;
	list = { 5, 10 };*/

	ASSERT( list.size() == 2 );
	ASSERT( *( list.begin() ) == 5 );
	ASSERT( *( list.begin() + 1 ) == 10 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( VariadicTest_Visitor )
{
	struct X
	{
		X() = default;
		X( const X & ) = delete;
		X( X && ) = delete;
	};
	struct Y
	{
		Y() = default;
		Y( const Y & ) = delete;
		Y( Y && ) = delete;
	};
	struct Z
	{
		Z() = default;
		Z( const Z & ) = delete;
		Z( Z && ) = delete;
	};
	
	using Visitor = VisitorTemplate::Visitor< X, Y, Z >;
	using DefaultVisitor = VisitorTemplate::DefaultVisitor< Visitor >;

	using WritableVisitor = VisitorTemplate::WritableVisitor< Visitor >;
	using DefaultWritableVisitor = VisitorTemplate::DefaultVisitor< WritableVisitor >;

	X x;
	Y y;
	Z z;

	const X cx;
	const Y cy;
	const Z cz;

	DefaultVisitor dv;
	static_assert( sizeof( dv ) == sizeof( void * ) );

	dv.visit( cx );
	void ( DefaultVisitor::* cxp )( const X & ) = & DefaultVisitor::visit;
	( dv .* cxp )( cx );

	dv.visit( cy );
	void ( DefaultVisitor::* cyp )( const Y & ) = & DefaultVisitor::visit;
	( dv .* cyp )( cy );

	dv.visit( cz );
	void ( DefaultVisitor::* czp )( const Z & ) = & DefaultVisitor::visit;
	( dv .* czp )( cz );

	DefaultWritableVisitor dwv;
	static_assert( sizeof( dwv ) == sizeof( void * ) );

	dwv.visit( x );
	void ( DefaultWritableVisitor::* xp )( X & ) = & DefaultWritableVisitor::visit;
	( dwv .* xp )( x );

	dwv.visit( y );
	void ( DefaultWritableVisitor::* yp )( Y & ) = & DefaultWritableVisitor::visit;
	( dwv .* yp )( y );

	dwv.visit( z );
	void ( DefaultWritableVisitor::* zp )( Z & ) = & DefaultWritableVisitor::visit;
	( dwv .* zp )( z );
}

//------------------------------------------------------------------------------

} // namespace Variadic::Test
