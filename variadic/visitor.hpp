#ifndef __VARIADIC_VISITOR_HPP__
#define __VARIADIC_VISITOR_HPP__

//------------------------------------------------------------------------------

namespace Variadic {

//------------------------------------------------------------------------------

namespace VisitorTemplateImpl {

/*----------------------------------------------------------------------------*/

template< typename ... >
using void_t = void; // std::void_t for C++17

/*----------------------------------------------------------------------------*/

template< typename ... >
class Visitor;

/*----------------------------------------------------------------------------*/

template< typename _Type >
class Visitor< _Type >
{

/*----------------------------------------------------------------------------*/

public:

/*----------------------------------------------------------------------------*/

	virtual ~Visitor() = default;

	using VisitType = const _Type;
	virtual void visit( VisitType & ) = 0;

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

template<
		typename _Type
	,	typename ... _Tail
>
class Visitor< _Type, _Tail... >
	:	public Visitor< _Tail... >
{

/*----------------------------------------------------------------------------*/

public:

/*----------------------------------------------------------------------------*/

	~Visitor() override = default;

	using NextVisitor = Visitor< _Tail... >;
	using NextVisitor::visit;

	using VisitType = const _Type;
	virtual void visit( VisitType & ) = 0;

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

template<
		typename _BaseVisitor
	,	typename _CurrentVisitor
	,	typename = void
>
class DefaultVisitor
	:	public _BaseVisitor
{

/*----------------------------------------------------------------------------*/

public:

/*----------------------------------------------------------------------------*/

	~DefaultVisitor() override = default;

	using VisitType = typename _CurrentVisitor::VisitType;
	void visit( VisitType & ) override
	{
	}

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

template<
		typename _BaseVisitor
	,	typename _CurrentVisitor
>
class DefaultVisitor<
		_BaseVisitor
	,	_CurrentVisitor
	,	void_t< typename _CurrentVisitor::NextVisitor >
>
	:	public DefaultVisitor<
				_BaseVisitor
			,	typename _CurrentVisitor::NextVisitor
		>
{

/*----------------------------------------------------------------------------*/

public:

/*----------------------------------------------------------------------------*/

	~DefaultVisitor() override = default;

	using NextDefaultVisitor = DefaultVisitor<
			_BaseVisitor
		,	typename _CurrentVisitor::NextVisitor
	>;
	using NextDefaultVisitor::visit;

	using VisitType = typename _CurrentVisitor::VisitType;
	void visit( VisitType & ) override
	{
	}

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

template< typename _CurrentVisitor, typename = void >
class WritableVisitor
{

/*----------------------------------------------------------------------------*/

public:

/*----------------------------------------------------------------------------*/

	virtual ~WritableVisitor() = default;

	using VisitType = std::remove_const_t< typename _CurrentVisitor::VisitType >;
	virtual void visit( VisitType & ) = 0;

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

template< typename _CurrentVisitor >
class WritableVisitor<
		_CurrentVisitor
	,	void_t< typename _CurrentVisitor::NextVisitor >
>
	:	public WritableVisitor< typename _CurrentVisitor::NextVisitor >
{

/*----------------------------------------------------------------------------*/

public:

/*----------------------------------------------------------------------------*/

	~WritableVisitor() override = default;

	using NextVisitor = WritableVisitor< typename _CurrentVisitor::NextVisitor >;
	using NextVisitor::visit;

	using VisitType = std::remove_const_t< typename _CurrentVisitor::VisitType >;
	virtual void visit( VisitType & ) = 0;

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

} // namespace VisitorTemplateImpl

/*----------------------------------------------------------------------------*/

namespace VisitorTemplate {

/*----------------------------------------------------------------------------*/

template< typename ... _Types >
class Visitor
	:	public VisitorTemplateImpl::Visitor< _Types... >
{
};

/*----------------------------------------------------------------------------*/

template< typename _BaseVisitor >
class WritableVisitor
	:	public VisitorTemplateImpl::WritableVisitor< _BaseVisitor >
{
};

/*----------------------------------------------------------------------------*/

template< typename _BaseVisitor >
class DefaultVisitor
	:	public VisitorTemplateImpl::DefaultVisitor<
				_BaseVisitor
			,	_BaseVisitor
		>
{
};

/*----------------------------------------------------------------------------*/

} // namespace VisitorTemplate

//------------------------------------------------------------------------------

} // namespace Variadic

//------------------------------------------------------------------------------

#endif // __VARIADIC_VISITOR_HPP__
