#ifndef __VARIADIC_SIZE_HPP__
#define __VARIADIC_SIZE_HPP__

//------------------------------------------------------------------------------

namespace Variadic {

//------------------------------------------------------------------------------

template< typename ... T >
struct VariadicSize
{
	static const int value = sizeof...( T );
};

//------------------------------------------------------------------------------

} // namespace Variadic

//------------------------------------------------------------------------------

#endif // __VARIADIC_SIZE_HPP__
